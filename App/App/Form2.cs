﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace App
{
    public partial class CreateForm : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public CreateForm()
        {
            InitializeComponent();
        }

        private void txtLoginUser_Enter(object sender, EventArgs e)
        {
            if (textLoginUser.Text == "Entrez votre identifiant")
            {
                textLoginUser.Text = "";
                textLoginUser.ForeColor = Color.Black;
            }
        }

        private void txtLoginUser_Leave(object sender, EventArgs e)
        {
            if (textLoginUser.Text == "")
            {
                textLoginUser.Text = "Entrez votre identifiant";
                textLoginUser.ForeColor = Color.LightGray;
            }
        }

        private void CreateForm_Load(object sender, EventArgs e)
        {
            textFirstName.Text = "Entrez votre nom";
            textLastName.Text = "Entrez votre prénom";
            textLoginUser.Text = "Entrez votre identifiant";
            textPasswordCreate.Text = "Entrez votre mot de passe";
            textPasswordConfirm.Text = "Confirmer votre mot de passe";
            textFirstName.ForeColor = Color.LightGray;
            textLastName.ForeColor = Color.LightGray;
            textLoginUser.ForeColor = Color.LightGray;
            textPasswordCreate.ForeColor = Color.LightGray;
            textPasswordConfirm.ForeColor = Color.LightGray;
        }

        private void txtPasswordCreate_Enter(object sender, EventArgs e)
        {
            if (textPasswordCreate.Text == "Entrez votre mot de passe")
            {
                textPasswordCreate.Text = "";
                textPasswordCreate.ForeColor = Color.Black;
                textPasswordCreate.PasswordChar = '*';
            }
        }

        private void txtPasswordCreate_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPasswordCreate.Text))
            {
                textPasswordCreate.Text = "Entrez votre mot de passe";
                textPasswordCreate.ForeColor = Color.LightGray;
                textPasswordCreate.PasswordChar = '\0';
            }
        }

        private void textPasswordConfirm_Enter(object sender, EventArgs e)
        {
            if (textPasswordConfirm.Text == "Confirmer votre mot de passe")
            {
                textPasswordConfirm.Text = "";
                textPasswordConfirm.ForeColor = Color.Black;
                textPasswordConfirm.PasswordChar = '*';
            }
        }

        private void textPasswordConfirm_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPasswordConfirm.Text))
            {
                textPasswordConfirm.Text = "Entrez votre mot de passe";
                textPasswordConfirm.ForeColor = Color.LightGray;
                textPasswordConfirm.PasswordChar = '\0';
            }
        }

        private void textname_Enter(object sender, EventArgs e)
        {
            if (textFirstName.Text == "Entrez votre nom")
            {
                textFirstName.Text = "";
                textFirstName.ForeColor = Color.Black;
            }
        }

        private void textname_Leave(object sender, EventArgs e)
        {
            if (textFirstName.Text == "")
            {
                textFirstName.Text = "Entrez votre nom";
                textFirstName.ForeColor = Color.LightGray;
            }
        }

        private void textLastName_Enter(object sender, EventArgs e)
        {
            if (textLastName.Text == "Entrez votre prénom")
            {
                textLastName.Text = "";
                textLastName.ForeColor = Color.Black;
            }
        }

        private void textLastName_Leave(object sender, EventArgs e)
        {
            if (textLastName.Text == "")
            {
                textLastName.Text = "Entrez votre prénom";
                textLastName.ForeColor = Color.LightGray;
            }
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            // Hide the login form2
            this.Hide();

            // Create and show Form1
            Loginform Loginform = new Loginform();
            Loginform.ShowDialog();

            // Close the application when Form3 is closed (optional)
            Application.Exit();
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            string username = textLoginUser.Text;
            string password = textPasswordCreate.Text;
            string confirmPassword = textPasswordConfirm.Text;
            string firstName = textFirstName.Text;
            string lastName = textLastName.Text;

            // Validate inputs
            if (string.IsNullOrEmpty(username) ||
                string.IsNullOrEmpty(password) ||
                string.IsNullOrEmpty(confirmPassword) ||
                string.IsNullOrEmpty(firstName) ||
                string.IsNullOrEmpty(lastName) ||
                username == "Entrez votre nom" ||
                password == "Entrez votre mot de passe" ||
                confirmPassword == "Confirmer votre mot de passe" ||
                firstName == "Entrez votre nom" ||
                lastName == "Entrez votre prénom")
            {
                MessageBox.Show("Veuillez remplir tous les champs.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // if password and confirm password isn't the same
            if (password != confirmPassword)
            {
                MessageBox.Show("Les mots de passe ne correspondent pas.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Insert user into the database
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();
           
            try
            {
                string profil = "Utilisateur";

                // Check if the username already exists
                string checkQuery = "SELECT COUNT(*) FROM users WHERE username = @username";
                using (SQLiteCommand checkCommand = new SQLiteCommand(checkQuery, connection))
                {
                    checkCommand.Parameters.AddWithValue("@username", username);
                    int count = Convert.ToInt32(checkCommand.ExecuteScalar());

                    if (count > 0)
                    {
                        MessageBox.Show("Ce nom d'utilisateur existe déjà. Choisissez un autre.","Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                // Check if the table 'users' is empty
                string checkEmptyQuery = "SELECT COUNT(*) FROM users";
                using (SQLiteCommand checkEmptyCommand = new SQLiteCommand(checkEmptyQuery, connection))
                {
                    int totalUsers = Convert.ToInt32(checkEmptyCommand.ExecuteScalar());

                    if (totalUsers == 0)
                    {
                        profil = "Administrateur";
                    }

                    // Insert the new user
                    string insertQuery = "INSERT INTO users (username, password, first_name, last_name, profil) " +
                                         "VALUES (@username, @password, @firstName, @lastName, @profil)";
                    using (SQLiteCommand insertCommand = new SQLiteCommand(insertQuery, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@username", username);
                        insertCommand.Parameters.AddWithValue("@password", password);
                        insertCommand.Parameters.AddWithValue("@firstName", firstName);
                        insertCommand.Parameters.AddWithValue("@lastName", lastName);
                        insertCommand.Parameters.AddWithValue("@profil", profil);

                        int rowsAffected = insertCommand.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            MessageBox.Show("Utilisateur inséré avec succès!", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            // Hide the login form2
                            this.Hide();

                            // Create and show Form1
                            Loginform Loginform = new Loginform();
                            Loginform.ShowDialog();

                            // Close the application when Form3 is closed (optional)
                            Application.Exit();
                        }
                        else
                        {
                            MessageBox.Show("Erreur lors de l'insertion de l'utilisateur.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CreateForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void CreateForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void CreateForm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }
    }
}
