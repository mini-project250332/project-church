﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Loginform;

namespace App
{
    public partial class Accueil : Form
    {
        public Accueil()
        {
            InitializeComponent();
        }

        private void Accueil_Load(object sender, EventArgs e)
        {
            int UserId = Session.UserId;
            string FirstName = Session.FirstName;
            string LastName = Session.LastName;

            textParaph.TextAlign = HorizontalAlignment.Center;
            textParaph.Text = "Bonjour " + FirstName + " " + LastName+ ",\r\n\r\n";
            textParaph.Enabled = false; 
            textParaph.ForeColor = Color.Black;
            textParaph.BackColor = Color.White;

            textEvengile.TextAlign = HorizontalAlignment.Center;
            textEvengile.Text = "\"Louez l'Eternel, car il est bon, Car sa misécorde dure à toujours\" \r\n" +
                "(Psaume 107:1)\r\n\r\n" +
                "\"Ankalazao Iaveh, fa tsara fo izy, mandrakizay ny famindram-pony\" \r\n (Salamo 107:1)";
            textEvengile.Enabled = false;
            textEvengile.ForeColor = Color.Black;
            textEvengile.BackColor = Color.White;

            textbapteme.TextAlign = HorizontalAlignment.Center;
            textbapteme.Text = "\"Izaho nanao batemy anareo tamin'ny rano, fa izy kosa hanao batemy anareo amin'ny Fanahy Masina\" \r\n (Marka 1,8)";
            textbapteme.Enabled = false;
            textbapteme.ForeColor = Color.Black;
            textbapteme.BackColor = Color.White;

            textcommunion.TextAlign = HorizontalAlignment.Center;
            textcommunion.Text = "\"Fa isaky ny mihinana ity mofo ity sy misotro amin'ity kalisy ity hianareo, dia manambara ny fahafatesan'ny Tompo mandra-pihaviny.\" \r\n (1 Korintiana 11,26)";
            textcommunion.Enabled = false;
            textcommunion.ForeColor = Color.Black;
            textcommunion.BackColor = Color.White;

            textconfirmation.TextAlign = HorizontalAlignment.Center;
            textconfirmation.Text = "\"Koa mandehana hianareo, mampianara ny firenena rehetra, manaova batemy azy amin'ny anaran'ny Ray sy ny Zanaka sy ny Fanahy Masina.\" \r\n (Matio 28,19)";
            textconfirmation.Enabled = false;
            textconfirmation.ForeColor = Color.Black;
            textconfirmation.BackColor = Color.White;

            CreateUsersTablePersonneIfNotExists();
            CreateUsersTableBaptemeIfNotExists();
            CreateUsersTableCommunionIfNotExists();
            CreateUsersTableConfirmationIfNotExists();
        }

        static bool DoesTableExist(SQLiteConnection connection, string tableName)
        {
            using (SQLiteCommand commandSQLite = new SQLiteCommand($"SELECT name FROM sqlite_master WHERE type='table' AND name='{tableName}'", connection))
            {
                using (SQLiteDataReader reader = commandSQLite.ExecuteReader())
                {
                    return reader.Read();
                }
            }
        }

        private void CreateUsersTablePersonneIfNotExists()
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                if (!DoesTableExist(connection, "personne"))
                {
                    CreatePersonneTable(connection);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de création de la table 'personne': " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static void CreatePersonneTable(SQLiteConnection connection)
        {
            string createTableQuery = "CREATE TABLE personne (id_personne INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "last_name TEXT, first_name TEXT, sexe TEXT, date_birth DATE, place_birth TEXT, father_name TEXT, " +
                "fathers_contact TEXT, mothers_name TEXT, mothers_contact TEXT, address TEXT)";

            using (SQLiteCommand commandSQLiteCreateUsers = new SQLiteCommand(createTableQuery, connection))
            {
                commandSQLiteCreateUsers.ExecuteNonQuery();
            }
        }

        private void CreateUsersTableBaptemeIfNotExists()
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                if (!DoesTableExist(connection, "bapteme"))
                {
                    CreateBaptemeTable(connection);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de création de la table 'bapteme': " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static void CreateBaptemeTable(SQLiteConnection connection)
        {
            string createTableQuery = "CREATE TABLE bapteme (id_bapteme INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "priest TEXT, godfather TEXT, date_bapteme DATE, id_personne INT)";

            using (SQLiteCommand commandSQLiteCreateUsers = new SQLiteCommand(createTableQuery, connection))
            {
                commandSQLiteCreateUsers.ExecuteNonQuery();
            }
        }

        private void CreateUsersTableCommunionIfNotExists()
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                if (!DoesTableExist(connection, "communion"))
                {
                    CreateCommunionTable(connection);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de création de la table 'communion': " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static void CreateCommunionTable(SQLiteConnection connection)
        {
            string createTableQuery = "CREATE TABLE communion (id_communion INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "priest TEXT, date_communion DATE, id_personne INT)";

            using (SQLiteCommand commandSQLiteCreateUsers = new SQLiteCommand(createTableQuery, connection))
            {
                commandSQLiteCreateUsers.ExecuteNonQuery();
            }
        }

        private void CreateUsersTableConfirmationIfNotExists()
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                if (!DoesTableExist(connection, "confirmation"))
                {
                    CreateConfirmationTable(connection);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de création de la table 'confirmation': " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static void CreateConfirmationTable(SQLiteConnection connection)
        {
            string createTableQuery = "CREATE TABLE confirmation (id_confirmation INTEGER PRIMARY KEY AUTOINCREMENT, " +
                 "priest TEXT, godfather TEXT, date_confirmation DATE, id_personne INT)";

            using (SQLiteCommand commandSQLiteCreateUsers = new SQLiteCommand(createTableQuery, connection))
            {
                commandSQLiteCreateUsers.ExecuteNonQuery();
            }
        }
    }
}
