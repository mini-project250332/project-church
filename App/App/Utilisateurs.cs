﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using static App.Loginform;

namespace App
{
    public partial class Utilisateurs : Form
    {
        public Utilisateurs()
        {
            InitializeComponent();
        }

        public static class EditUserid
        {
            public static int UserId { get; set; }
        }

        private void OpenForm()
        {
            AjoutUtilisateur ajoutUtilisateur = new AjoutUtilisateur(this);
            ajoutUtilisateur.Show();

            ModifierUtilisateur modifierUtilisateur = new ModifierUtilisateur(this);
            modifierUtilisateur.Show();
        }

        private void Utilisateurs_Load(object sender, EventArgs e)
        {
            show_data_utilisateur();
                      
            showhide_button_action();
        }

        public void show_data_utilisateur ()
        {
            table_utilisateurs.Rows.Clear();
            table_utilisateurs.ReadOnly = true;

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();
            
            try
            {
                string query = "SELECT * FROM users";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        while (readerList.Read())
                        {
                            object[] rowData = new object[readerList.FieldCount];
                            readerList.GetValues(rowData);

                            table_utilisateurs.Rows.Add(rowData);
                            table_utilisateurs.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void showhide_button_action()
        {
            string Profil = Session.Profil;

            if (Profil == "Administrateur")
            {
                button_ajouter.Visible = true;
                button_modifier.Visible = true;
                button_supprimer.Visible = true;
            }
            else
            {
                button_ajouter.Visible = false;
                button_modifier.Visible = false;
                button_supprimer.Visible = false;
                label_alert_user.Visible = true;
            }
        }

        private void table_utilisateurs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                DataGridViewRow selectedRow = table_utilisateurs.Rows[e.RowIndex];
                selectedRow.Selected = true;
            }
        }

        private string GetSelectedRowID()
        {
            if (table_utilisateurs.SelectedRows.Count > 0)
            {
                string selectedID = table_utilisateurs.SelectedRows[0].Cells[0].Value.ToString();
                return selectedID;
            }
            else
            {
                return null;
            }
        }

        private void button_Modifier_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                EditUserid.UserId = Convert.ToInt32(selectedID);

                ModifierUtilisateur modifierUtilisateur = new ModifierUtilisateur(this);
                modifierUtilisateur.ShowDialog();
            }
        }

        private void button_supprimer_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();
            string userid = Session.UserId.ToString();

            if (selectedID == userid)
            {
                MessageBox.Show("Vous ne pouvez pas supprimer cette ligne .", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DialogResult result = MessageBox.Show("Voulez-vous vraiment supprimer cet utilisateur ?", "Confirmation de suppression", MessageBoxButtons.YesNo);
                
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        string deleteQuery = "DELETE FROM users WHERE id = @id";
                       
                        using (SQLiteCommand deleteCommand = new SQLiteCommand(deleteQuery, connection))
                        {
                            deleteCommand.Parameters.AddWithValue("@id", selectedID);

                            int rowsAffected = deleteCommand.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                MessageBox.Show("L'utilisateur a été supprimé avec succès!", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                show_data_utilisateur();
                            }
                            else
                            {
                                MessageBox.Show("Échec de la suppression de l'utilisateur.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erreur lors de la suppression de l'utilisateur: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void button_ajouter_Click(object sender, EventArgs e)
        {
            AjoutUtilisateur AjoutUtilisateur = new AjoutUtilisateur(this);
            AjoutUtilisateur.ShowDialog(this);
        }
    }
}
