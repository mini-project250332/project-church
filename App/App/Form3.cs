﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using System.Data.SQLite;

namespace App
{
    public partial class forgetPassword : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public forgetPassword()
        {
            InitializeComponent();
        }

        public static class PassId
        {
            public static int UserId { get; set; }
        }

        private void forgetPassword_Load(object sender, EventArgs e)
        {
            textParaph.Text = "Veuillez entrez votre identifiant pour faire le changement de mot de passe.";
            textParaph.Enabled = false;
            textParaph.BackColor = Color.White;
            checkUsername.Text = "Entrez votre identifiant";
            checkUsername.ForeColor = Color.LightGray;
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            // Hide the login form3
            this.Hide();

            // Create and show Form1
            Loginform Loginform = new Loginform();
            Loginform.ShowDialog();

            // Close the application when Form3 is closed (optional)
            Application.Exit();
        }

        private void checkUsername_Enter(object sender, EventArgs e)
        {
            if (checkUsername.Text == "Entrez votre identifiant")
            {
                checkUsername.Text = "";
                checkUsername.ForeColor = Color.Black;
            }
        }

        private void checkUsername_Leave(object sender, EventArgs e)
        {
            if (checkUsername.Text == "")
            {
                checkUsername.Text = "Entrez votre identifiant";
                checkUsername.ForeColor = Color.LightGray;
            }
        }

        private void button_verifier_Click(object sender, EventArgs e)
        {
            string username = checkUsername.Text;

            if (string.IsNullOrEmpty(username) || username == "Entrez votre identifiant")
            {
                MessageBox.Show("Veuillez entrez votre identifiant.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();
            
            try
            {
                string query = "SELECT * FROM users WHERE username = @username";
                using (SQLiteCommand commandCheck = new SQLiteCommand(query, connection))
                {
                    commandCheck.Parameters.AddWithValue("@username", username);

                    using (SQLiteDataReader reader = commandCheck.ExecuteReader())
                    {
                        // Successful
                        if (reader.Read())
                        {
                            
                            PassId.UserId = Convert.ToInt32(reader["id"]);

                            // Hide the login form
                            this.Hide();

                            // Create and show Form4
                            newPassword newPassword = new newPassword();
                            newPassword.ShowDialog();

                            // Close the application when Form4 is closed (optional)
                            Application.Exit();
                        }
                        // Invalid credentials
                        else
                        {
                            MessageBox.Show("Votre identifiant ne figure pas dans la base de donnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void forgetPassword_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void forgetPassword_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void forgetPassword_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }
    }
}
