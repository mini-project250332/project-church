﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace App
{
    public partial class AjoutPersonne : Form
    {
        private Personnes _formPersonneReference;

        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public AjoutPersonne(Personnes formPersonneReference)
        {
            InitializeComponent();

            _formPersonneReference = formPersonneReference;
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void AjoutPersonne_Load(object sender, EventArgs e)
        {
            textLastName.Text = "Entrez le nom de la personne";
            textLastName.ForeColor = Color.LightGray;
            textFirstName.Text = "Entrez le prénom de la personne";
            textFirstName.ForeColor = Color.LightGray;
            genre_select.SelectedItem = "";
            genre_select.DropDownStyle = ComboBoxStyle.DropDownList;
            textDadName.Text = "Entrez le Nom et prénom du père";
            textDadName.ForeColor = Color.LightGray;
            textMomName.Text = "Entrez le Nom et prénom de la mère";
            textMomName.ForeColor = Color.LightGray;
            textContactDad.Text = "Numéro téléphone du père";
            textContactDad.ForeColor = Color.LightGray;
            textContactMom.Text = "Numéro téléphone de la mère";
            textContactMom.ForeColor = Color.LightGray;
            textPlaceBirth.Text = "Lieu de naissance de la personne";
            textPlaceBirth.ForeColor = Color.LightGray;
            textAddress.Text = "Adresse de la personne";
            textAddress.ForeColor = Color.LightGray;
        }

        private void textLastName_Enter(object sender, EventArgs e)
        {
            if (textLastName.Text == "Entrez le nom de la personne")
            {
                textLastName.Text = "";
                textLastName.ForeColor = Color.Black;
            }
        }

        private void textLastName_Leave(object sender, EventArgs e)
        {
            if (textLastName.Text == "")
            {
                textLastName.Text = "Entrez le nom de la personne";
                textLastName.ForeColor = Color.LightGray;
            }
        }

        private void textFirstName_Enter(object sender, EventArgs e)
        {
            if (textFirstName.Text == "Entrez le prénom de la personne")
            {
                textFirstName.Text = "";
                textFirstName.ForeColor = Color.Black;
            }
        }

        private void textFirstName_Leave(object sender, EventArgs e)
        {
            if (textFirstName.Text == "")
            {
                textFirstName.Text = "Entrez le prénom de la personne";
                textFirstName.ForeColor = Color.LightGray;
            }
        }

        private void textDadName_Enter(object sender, EventArgs e)
        {
            if (textDadName.Text == "Entrez le Nom et prénom du père")
            {
                textDadName.Text = "";
                textDadName.ForeColor = Color.Black;
            }
        }

        private void textDadName_Leave(object sender, EventArgs e)
        {
            if (textDadName.Text == "")
            {
                textDadName.Text = "Entrez le Nom et prénom du père";
                textDadName.ForeColor = Color.LightGray;
            }
        }

        private void textContactDad_Enter(object sender, EventArgs e)
        {
            if (textContactDad.Text == "Numéro téléphone du père")
            {
                textContactDad.Text = "";
                textContactDad.ForeColor = Color.Black;
            }
        }

        private void textContactDad_Leave(object sender, EventArgs e)
        {
            if (textContactDad.Text == "")
            {
                textContactDad.Text = "Numéro téléphone du père";
                textContactDad.ForeColor = Color.LightGray;
            }
        }

        private void textMomName_Enter(object sender, EventArgs e)
        {
            if (textMomName.Text == "Entrez le Nom et prénom de la mère")
            {
                textMomName.Text = "";
                textMomName.ForeColor = Color.Black;
            }
        }

        private void textMomName_Leave(object sender, EventArgs e)
        {
            if (textMomName.Text == "")
            {
                textMomName.Text = "Entrez le Nom et prénom de la mère";
                textMomName.ForeColor = Color.LightGray;
            }
        }

        private void textContactMom_Enter(object sender, EventArgs e)
        {
            if (textContactMom.Text == "Numéro téléphone de la mère")
            {
                textContactMom.Text = "";
                textContactMom.ForeColor = Color.Black;
            }
        }

        private void textContactMom_Leave(object sender, EventArgs e)
        {
            if (textContactMom.Text == "")
            {
                textContactMom.Text = "Numéro téléphone de la mère";
                textContactMom.ForeColor = Color.LightGray;
            }
        }

        private void textPlaceBirth_Leave(object sender, EventArgs e)
        {
            if (textPlaceBirth.Text == "")
            {
                textPlaceBirth.Text = "Lieu de naissance de la personne";
                textPlaceBirth.ForeColor = Color.LightGray;
            }
        }

        private void textPlaceBirth_Enter(object sender, EventArgs e)
        {
            if (textPlaceBirth.Text == "Lieu de naissance de la personne")
            {
                textPlaceBirth.Text = "";
                textPlaceBirth.ForeColor = Color.Black;
            }
        }

        private void textAddress_Enter(object sender, EventArgs e)
        {
            if (textAddress.Text == "Adresse de la personne")
            {
                textAddress.Text = "";
                textAddress.ForeColor = Color.Black;
            }
        }

        private void textAddress_Leave(object sender, EventArgs e)
        {
            if (textAddress.Text == "")
            {
                textAddress.Text = "Adresse de la personne";
                textAddress.ForeColor = Color.LightGray;
            }
        }

        private void textContactDad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textContactMom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            string firstName = textFirstName.Text;
            string lastName = textLastName.Text;
            string sexe = genre_select.SelectedItem?.ToString();
            string father_name = textDadName.Text;
            string fathers_contact = textContactDad.Text;
            string mothers_name = textMomName.Text;
            string mothers_contact = textContactMom.Text;
            DateTime date_birth = dateBirth.Value.Date;
            string place_birth = textPlaceBirth.Text;
            string address = textAddress.Text;

            if (string.IsNullOrEmpty(lastName) || lastName == "Entrez le nom de la personne")
            {
                MessageBox.Show("Le champs nom de la personne ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(firstName) ||  firstName == "Entrez le prénom de la personne")
            {
                MessageBox.Show("Le champs prénom de la personne ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(sexe))
            {
                MessageBox.Show("Vous devez choisir un genre dans la liste déroulante.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (dateBirth == null || dateBirth.Value == DateTime.MinValue)
            {
                MessageBox.Show("Vous devez choisir une date de naissance.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(place_birth) || place_birth == "Lieu de naissance de la personne")
            {
                MessageBox.Show("Le champs Lieu de naissance de la personne ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if ((string.IsNullOrEmpty(father_name) && string.IsNullOrEmpty(mothers_name)) || (father_name == "Entrez le Nom et prénom du père" && mothers_name == "Entrez le Nom et prénom de la mère"))
            {
                MessageBox.Show("La personne doit avoir au moins un père ou une mère.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(address) || address == "Adresse de la personne")
            {
                MessageBox.Show("L'Adresse de la personne ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (father_name == "Entrez le Nom et prénom du père")
            {
                father_name = "";
            }

            if (fathers_contact== "Numéro téléphone du père")
            {
                fathers_contact = "";
            }

            if (mothers_name == "Entrez le Nom et prénom de la mère")
            {
                mothers_name = "";
            }

            if (mothers_contact == "Numéro téléphone de la mère")
            {
                mothers_contact = "";
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try {
                string insertQuery = "INSERT INTO personne (last_name, first_name, sexe, father_name, fathers_contact, mothers_name, mothers_contact, date_birth, place_birth, address) " +
                                        "VALUES (@last_name, @first_name, @sexe, @father_name, @fathers_contact, @mothers_name, @mothers_contact, @date_birth, @place_birth, @address)";

                string CheckPersonneQuery = "SELECT COUNT(*) FROM personne WHERE last_name = @lastname AND first_name = @firstname";

                using (SQLiteCommand commandCheck = new SQLiteCommand(CheckPersonneQuery, connection))
                {
                    commandCheck.Parameters.AddWithValue("@lastname", lastName);
                    commandCheck.Parameters.AddWithValue("@firstname", firstName);

                    int count = Convert.ToInt32(commandCheck.ExecuteScalar());

                    if (count > 0)
                    {
                        MessageBox.Show("Cette personne existe déjà dans la base de données.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return; 
                    }

                    using (SQLiteCommand insertCommand = new SQLiteCommand(insertQuery, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@last_name", lastName);
                        insertCommand.Parameters.AddWithValue("@first_name", firstName);
                        insertCommand.Parameters.AddWithValue("@sexe", sexe);
                        insertCommand.Parameters.AddWithValue("@father_name", father_name);
                        insertCommand.Parameters.AddWithValue("@fathers_contact", fathers_contact);
                        insertCommand.Parameters.AddWithValue("@mothers_name", mothers_name);
                        insertCommand.Parameters.AddWithValue("@mothers_contact", mothers_contact);
                        insertCommand.Parameters.AddWithValue("@date_birth", date_birth);
                        insertCommand.Parameters.AddWithValue("@place_birth", place_birth);
                        insertCommand.Parameters.AddWithValue("@address", address);

                        int rowsAffected = insertCommand.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            MessageBox.Show("Personne insérée avec succès!", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            this.Hide();

                            _formPersonneReference.show_data_personne();
                        }
                        else
                        {
                            MessageBox.Show("Erreur lors de l'insertion de la Personne.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
