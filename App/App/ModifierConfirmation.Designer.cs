﻿namespace App
{
    partial class ModifierConfirmation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModifierConfirmation));
            this.panel_moved = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button_exit = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.dateConfirmation = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.select_personne = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textPriestName = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textGodFather = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button_retour = new System.Windows.Forms.Button();
            this.button_enregistrer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_moved.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_moved
            // 
            this.panel_moved.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.panel_moved.Controls.Add(this.label2);
            this.panel_moved.Controls.Add(this.button_exit);
            this.panel_moved.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_moved.Location = new System.Drawing.Point(0, 0);
            this.panel_moved.Margin = new System.Windows.Forms.Padding(2);
            this.panel_moved.Name = "panel_moved";
            this.panel_moved.Size = new System.Drawing.Size(628, 32);
            this.panel_moved.TabIndex = 8;
            this.panel_moved.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseDown);
            this.panel_moved.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseMove);
            this.panel_moved.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(343, 19);
            this.label2.TabIndex = 78;
            this.label2.Text = "Modification des informations de la confirmation";
            // 
            // button_exit
            // 
            this.button_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            this.button_exit.FlatAppearance.BorderSize = 0;
            this.button_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_exit.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_exit.ForeColor = System.Drawing.Color.White;
            this.button_exit.Image = ((System.Drawing.Image)(resources.GetObject("button_exit.Image")));
            this.button_exit.Location = new System.Drawing.Point(596, 0);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(31, 32);
            this.button_exit.TabIndex = 78;
            this.button_exit.UseVisualStyleBackColor = false;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel11.Controls.Add(this.dateConfirmation);
            this.panel11.Location = new System.Drawing.Point(316, 262);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(260, 25);
            this.panel11.TabIndex = 108;
            // 
            // dateConfirmation
            // 
            this.dateConfirmation.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateConfirmation.CustomFormat = "dd/MM/yyyy";
            this.dateConfirmation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateConfirmation.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateConfirmation.Location = new System.Drawing.Point(1, 1);
            this.dateConfirmation.Name = "dateConfirmation";
            this.dateConfirmation.Size = new System.Drawing.Size(257, 22);
            this.dateConfirmation.TabIndex = 107;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.pictureBox8);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.select_personne);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.textPriestName);
            this.panel2.Controls.Add(this.pictureBox6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.textGodFather);
            this.panel2.Controls.Add(this.pictureBox5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.button_retour);
            this.panel2.Controls.Add(this.button_enregistrer);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(6, 39);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(615, 399);
            this.panel2.TabIndex = 9;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(275, 260);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(35, 27);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 105;
            this.pictureBox8.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(86, 268);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(169, 19);
            this.label10.TabIndex = 100;
            this.label10.Text = "Date de la confirmation :";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel9.Location = new System.Drawing.Point(277, 293);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(307, 1);
            this.panel9.TabIndex = 97;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(108, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 19);
            this.label5.TabIndex = 92;
            this.label5.Text = "Nom de la personne :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(235, 19);
            this.label4.TabIndex = 91;
            this.label4.Text = "Nom du parrain ou de la marraine :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(145, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 19);
            this.label3.TabIndex = 90;
            this.label3.Text = "Nom du prêtre :";
            // 
            // select_personne
            // 
            this.select_personne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.select_personne.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.select_personne.ForeColor = System.Drawing.SystemColors.ControlText;
            this.select_personne.Location = new System.Drawing.Point(316, 204);
            this.select_personne.Margin = new System.Windows.Forms.Padding(2);
            this.select_personne.Name = "select_personne";
            this.select_personne.Size = new System.Drawing.Size(267, 25);
            this.select_personne.TabIndex = 77;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(277, 204);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 76;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel6.Location = new System.Drawing.Point(275, 236);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(307, 1);
            this.panel6.TabIndex = 75;
            // 
            // textPriestName
            // 
            this.textPriestName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPriestName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPriestName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textPriestName.Location = new System.Drawing.Point(316, 91);
            this.textPriestName.Name = "textPriestName";
            this.textPriestName.Size = new System.Drawing.Size(266, 19);
            this.textPriestName.TabIndex = 74;
            this.textPriestName.Enter += new System.EventHandler(this.textPriestName_Enter);
            this.textPriestName.Leave += new System.EventHandler(this.textPriestName_Leave);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(275, 88);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(35, 27);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 73;
            this.pictureBox6.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel5.Location = new System.Drawing.Point(275, 120);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(307, 1);
            this.panel5.TabIndex = 72;
            // 
            // textGodFather
            // 
            this.textGodFather.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textGodFather.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textGodFather.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textGodFather.Location = new System.Drawing.Point(316, 149);
            this.textGodFather.Name = "textGodFather";
            this.textGodFather.Size = new System.Drawing.Size(266, 19);
            this.textGodFather.TabIndex = 71;
            this.textGodFather.Enter += new System.EventHandler(this.textGodFather_Enter);
            this.textGodFather.Leave += new System.EventHandler(this.textGodFather_Leave);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(275, 145);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(35, 27);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 70;
            this.pictureBox5.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel4.Location = new System.Drawing.Point(275, 177);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(307, 1);
            this.panel4.TabIndex = 69;
            // 
            // button_retour
            // 
            this.button_retour.BackColor = System.Drawing.Color.Firebrick;
            this.button_retour.FlatAppearance.BorderSize = 0;
            this.button_retour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_retour.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_retour.ForeColor = System.Drawing.Color.White;
            this.button_retour.Location = new System.Drawing.Point(325, 335);
            this.button_retour.Name = "button_retour";
            this.button_retour.Size = new System.Drawing.Size(117, 41);
            this.button_retour.TabIndex = 68;
            this.button_retour.Text = "Retour";
            this.button_retour.UseVisualStyleBackColor = false;
            this.button_retour.Click += new System.EventHandler(this.button_retour_Click);
            // 
            // button_enregistrer
            // 
            this.button_enregistrer.BackColor = System.Drawing.SystemColors.Highlight;
            this.button_enregistrer.FlatAppearance.BorderSize = 0;
            this.button_enregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_enregistrer.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_enregistrer.ForeColor = System.Drawing.Color.White;
            this.button_enregistrer.Location = new System.Drawing.Point(459, 335);
            this.button_enregistrer.Name = "button_enregistrer";
            this.button_enregistrer.Size = new System.Drawing.Size(117, 41);
            this.button_enregistrer.TabIndex = 62;
            this.button_enregistrer.Text = "Enregistrer";
            this.button_enregistrer.UseVisualStyleBackColor = false;
            this.button_enregistrer.Click += new System.EventHandler(this.button_enregistrer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(173, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 29);
            this.label1.TabIndex = 57;
            this.label1.Text = "Veuillez remplir les champs :";
            // 
            // ModifierConfirmation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(628, 447);
            this.Controls.Add(this.panel_moved);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ModifierConfirmation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ModifierConfirmation";
            this.Load += new System.EventHandler(this.ModifierConfirmation_Load);
            this.panel_moved.ResumeLayout(false);
            this.panel_moved.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_moved;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.DateTimePicker dateConfirmation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox select_personne;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textPriestName;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textGodFather;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button_retour;
        private System.Windows.Forms.Button button_enregistrer;
        private System.Windows.Forms.Label label1;
    }
}