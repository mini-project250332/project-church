﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using static System.Collections.Specialized.BitVector32;

namespace App
{
    public partial class Loginform : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public Loginform()
        {
            InitializeComponent();
        }

        // Class session
        public static class Session
        {
            public static int UserId { get; set; }
            public static string FirstName { get; set; }
            public static string LastName { get; set; }
            public static string Profil { get; set; }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtUserName.Text = "Entrez votre identifiant";
            txtpassword.Text = "Entrez votre mot de passe";
            txtUserName.ForeColor = Color.LightGray;
            txtpassword.ForeColor = Color.LightGray;

            // Create the table if it does not exist
            CreateUsersTableIfNotExists();
        }

        private void CreateUsersTableIfNotExists()
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();
            
            try
            {
                // Check if the table 'users' exists
                if (!DoesTableExist(connection, "users"))
                {
                    CreateUsersTable(connection);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de création de la table 'users': " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static void CreateUsersTable(SQLiteConnection connection)
        {
            string createTableQuery = "CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, first_name TEXT, last_name , profil TEXT, username TEXT, password TEXT)";

            using (SQLiteCommand commandSQLiteCreateUsers = new SQLiteCommand(createTableQuery, connection))
            {
                commandSQLiteCreateUsers.ExecuteNonQuery();
            }
        }

        static bool DoesTableExist(SQLiteConnection connection, string tableName)
        {
            using (SQLiteCommand commandSQLite = new SQLiteCommand($"SELECT name FROM sqlite_master WHERE type='table' AND name='{tableName}'", connection))
            {
                using (SQLiteDataReader reader = commandSQLite.ExecuteReader())
                {
                    return reader.Read();
                }
            }
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            if (txtUserName.Text == "Entrez votre identifiant")
            {
                txtUserName.Text = "";
                txtUserName.ForeColor = Color.Black;
            }
        }

        private void txtUserName_Leave(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
            {
                txtUserName.Text = "Entrez votre identifiant";
                txtUserName.ForeColor = Color.LightGray;
            }
        }

        private void txtpassword_Enter(object sender, EventArgs e)
        {
            if (txtpassword.Text == "Entrez votre mot de passe")
            {
                txtpassword.Text = "";
                txtpassword.ForeColor = Color.Black;
                txtpassword.PasswordChar = '*';
            }
        }

        private void txtpassword_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtpassword.Text))
            {
                txtpassword.Text = "Entrez votre mot de passe";
                txtpassword.ForeColor = Color.LightGray;
                txtpassword.PasswordChar = '\0';
            }
        }

        private void button_connection_Click(object sender, EventArgs e)
        {
            string username = txtUserName.Text;
            string password = txtpassword.Text;

            // Validate username and password (you may want to add more robust validation)
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || username == "Entrez votre identifiant" || password == "Entrez votre mot de passe")
            {
                MessageBox.Show("Veuillez entrer un Identifiant et un mot de passe.","Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();
           
            try
            {
                string query = "SELECT * FROM users WHERE username = @username AND password = @password";

                using (SQLiteCommand commandLogin = new SQLiteCommand(query, connection))
                {
                    commandLogin.Parameters.AddWithValue("@username", username);
                    commandLogin.Parameters.AddWithValue("@password", password);

                    using (SQLiteDataReader reader = commandLogin.ExecuteReader())
                    {
                        // Successful login
                        if (reader.Read())
                        {
                            Session.UserId = Convert.ToInt32(reader["id"]);
                            Session.FirstName = reader["first_name"].ToString();
                            Session.LastName = reader["last_name"].ToString();
                            Session.Profil = reader["profil"].ToString();

                            // Hide the login form
                            this.Hide();

                            // Create and show MainForm
                            MainForm MainForm = new MainForm();
                            MainForm.ShowDialog();

                            // Close the application when Form3 is closed (optional)
                            Application.Exit();
                        }
                        // Invalid credentials
                        else
                        {
                            MessageBox.Show("Identifiant ou mot de passe incorrect.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label_inscription(object sender, EventArgs e)
        {
            // Hide the login form1
            this.Hide();

            // Create and show Form2
            CreateForm form2 = new CreateForm();
            form2.ShowDialog();
        }

        private void forget_password_Click(object sender, EventArgs e)
        {
            // Hide the login form1
            this.Hide();

            // Create and show Form3
            forgetPassword form3 = new forgetPassword();
            form3.ShowDialog();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Loginform_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void Loginform_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void Loginform_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }
    }
}
