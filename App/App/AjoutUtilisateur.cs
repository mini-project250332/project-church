﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    public partial class AjoutUtilisateur : Form
    {
        private Utilisateurs _formUserReference;

        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public AjoutUtilisateur(Utilisateurs formUserReference)
        {
            InitializeComponent();

            _formUserReference = formUserReference;
        }

        private void AjoutUtilisateur_Load(object sender, EventArgs e)
        {
            textFirstName.Text = "Entrez votre nom";
            textLastName.Text = "Entrez votre prénom";
            textLoginUser.Text = "Entrez votre identifiant";
            textPasswordCreate.Text = "Entrez votre mot de passe";
            textPasswordConfirm.Text = "Confirmer votre mot de passe";
            textFirstName.ForeColor = Color.LightGray;
            textLastName.ForeColor = Color.LightGray;
            textLoginUser.ForeColor = Color.LightGray;
            textPasswordCreate.ForeColor = Color.LightGray;
            textPasswordConfirm.ForeColor = Color.LightGray;

            profil_select.SelectedItem = "Utilisateur";
            profil_select.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void textFirstName_Enter(object sender, EventArgs e)
        {
            if (textFirstName.Text == "Entrez votre nom")
            {
                textFirstName.Text = "";
                textFirstName.ForeColor = Color.Black;
            }
        }

        private void textFirstName_Leave(object sender, EventArgs e)
        {
            if (textFirstName.Text == "")
            {
                textFirstName.Text = "Entrez votre nom";
                textFirstName.ForeColor = Color.LightGray;
            }
        }

        private void textLastName_Leave(object sender, EventArgs e)
        {
            if (textLastName.Text == "")
            {
                textLastName.Text = "Entrez votre prénom";
                textLastName.ForeColor = Color.LightGray;
            }
        }

        private void textLastName_Enter(object sender, EventArgs e)
        {
            if (textLastName.Text == "Entrez votre prénom")
            {
                textLastName.Text = "";
                textLastName.ForeColor = Color.Black;
            }
        }

        private void textLoginUser_Leave(object sender, EventArgs e)
        {
            if (textLoginUser.Text == "")
            {
                textLoginUser.Text = "Entrez votre identifiant";
                textLoginUser.ForeColor = Color.LightGray;
            }
        }

        private void textLoginUser_Enter(object sender, EventArgs e)
        {
            if (textLoginUser.Text == "Entrez votre identifiant")
            {
                textLoginUser.Text = "";
                textLoginUser.ForeColor = Color.Black;
            }
        }

        private void textPasswordCreate_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPasswordCreate.Text))
            {
                textPasswordCreate.Text = "Entrez votre mot de passe";
                textPasswordCreate.ForeColor = Color.LightGray;
                textPasswordCreate.PasswordChar = '\0';
            }
        }

        private void textPasswordCreate_Enter(object sender, EventArgs e)
        {
            if (textPasswordCreate.Text == "Entrez votre mot de passe")
            {
                textPasswordCreate.Text = "";
                textPasswordCreate.ForeColor = Color.Black;
                textPasswordCreate.PasswordChar = '*';
            }
        }

        private void textPasswordConfirm_Enter(object sender, EventArgs e)
        {
            if (textPasswordConfirm.Text == "Confirmer votre mot de passe")
            {
                textPasswordConfirm.Text = "";
                textPasswordConfirm.ForeColor = Color.Black;
                textPasswordConfirm.PasswordChar = '*';
            }
        }

        private void textPasswordConfirm_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPasswordConfirm.Text))
            {
                textPasswordConfirm.Text = "Entrez votre mot de passe";
                textPasswordConfirm.ForeColor = Color.LightGray;
                textPasswordConfirm.PasswordChar = '\0';
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            string username = textLoginUser.Text;
            string password = textPasswordCreate.Text;
            string confirmPassword = textPasswordConfirm.Text;
            string firstName = textFirstName.Text;
            string lastName = textLastName.Text;
            string profil = profil_select.SelectedItem?.ToString();

            // Validate inputs
            if (string.IsNullOrEmpty(username) ||
                string.IsNullOrEmpty(password) ||
                string.IsNullOrEmpty(confirmPassword) ||
                string.IsNullOrEmpty(firstName) ||
                string.IsNullOrEmpty(lastName) ||
                username == "Entrez votre nom" ||
                password == "Entrez votre mot de passe" ||
                confirmPassword == "Confirmer votre mot de passe" ||
                firstName == "Entrez votre nom" ||
                lastName == "Entrez votre prénom")
            {
                MessageBox.Show("Veuillez remplir tous les champs.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // if password and confirm password isn't the same
            if (password != confirmPassword)
            {
                MessageBox.Show("Les mots de passe ne correspondent pas.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Insert user into the database
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                // Check if the username already exists
                string checkQuery = "SELECT COUNT(*) FROM users WHERE username = @username";
                using (SQLiteCommand checkCommand = new SQLiteCommand(checkQuery, connection))
                {
                    checkCommand.Parameters.AddWithValue("@username", username);
                    int count = Convert.ToInt32(checkCommand.ExecuteScalar());

                    if (count > 0)
                    {
                        MessageBox.Show("Ce nom d'utilisateur existe déjà. Choisissez un autre.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    // Insert the new user
                    string insertQuery = "INSERT INTO users (username, password, first_name, last_name, profil) " +
                                         "VALUES (@username, @password, @firstName, @lastName, @profil)";
                    using (SQLiteCommand insertCommand = new SQLiteCommand(insertQuery, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@username", username);
                        insertCommand.Parameters.AddWithValue("@password", password);
                        insertCommand.Parameters.AddWithValue("@firstName", firstName);
                        insertCommand.Parameters.AddWithValue("@lastName", lastName);
                        insertCommand.Parameters.AddWithValue("@profil", profil);

                        int rowsAffected = insertCommand.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            MessageBox.Show("Utilisateur inséré avec succès!", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            this.Hide();

                            _formUserReference.show_data_utilisateur();
                        }
                        else
                        {
                            MessageBox.Show("Erreur lors de l'insertion de l'utilisateur.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }
    }
}
