﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    public partial class MainForm : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        bool sidebarExpand;

        private Point initialPanelLocation;

        public MainForm()
        {
            InitializeComponent();

            initialPanelLocation = mainPanel.Location;
            SidePanel.Height = menu_welcome.Height;
            ActivateButton(menu_welcome);
            load_form(new Accueil());
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        private void Menu_moved_Click(object sender, EventArgs e)
        {
            timerSideBar.Start();
        }

        private void timerSideBar_Tick(object sender, EventArgs e)
        {
            if (sidebarExpand)
            {
                SideBar.Width -= 10;
                mainPanel.Left -= 10;

                if (SideBar.Width == SideBar.MinimumSize.Width)
                {
                    mainPanel.Width = mainPanel.MaximumSize.Width;
                    mainPanel.Left += 3;
                    sidebarExpand = false;
                    timerSideBar.Stop();
                }
            }
            else
            {
                SideBar.Width += 10;
                mainPanel.Left += 10;

                if (SideBar.Width == SideBar.MaximumSize.Width)
                {
                    mainPanel.Location = initialPanelLocation;
                    mainPanel.Width = mainPanel.MinimumSize.Width;
                    sidebarExpand = true;
                    timerSideBar.Stop();
                }
            }
        }

        private void menu_welcome_Click(object sender, EventArgs e)
        {
            SidePanel.Height = menu_welcome.Height;
            SidePanel.Top = menu_welcome.Top;

            ActivateButton(menu_welcome);
            load_form(new Accueil());
        }

        private void menu_list_bapteme_Click(object sender, EventArgs e)
        {
            SidePanel.Height = menu_list_bapteme.Height;
            SidePanel.Top = menu_list_bapteme.Top;

            ActivateButton(menu_list_bapteme);
            load_form(new Bapteme());
        }

        private void menu_list_communion_Click(object sender, EventArgs e)
        {
            SidePanel.Height = menu_list_communion.Height;
            SidePanel.Top = menu_list_communion.Top;

            ActivateButton(menu_list_communion);
            load_form(new Communion());
        }

        private void menu_list_confirmation_Click(object sender, EventArgs e)
        {
            SidePanel.Height = menu_list_confirmation.Height;
            SidePanel.Top = menu_list_confirmation.Top;

            ActivateButton(menu_list_confirmation);
            load_form(new Confirmation());
        }

        private void menu_list_user_Click(object sender, EventArgs e)
        {
            SidePanel.Height = menu_list_user.Height;
            SidePanel.Top = menu_list_user.Top;

            ActivateButton(menu_list_user);
            load_form(new Utilisateurs());
        }

        private void menu_personne_Click(object sender, EventArgs e)
        {
            SidePanel.Height = menu_personne.Height;
            SidePanel.Top = menu_personne.Top;

            ActivateButton(menu_personne);
            load_form(new Personnes());
        }

        private void menu_deconnexion_Click(object sender, EventArgs e)
        {
            SidePanel.Height = menu_deconnexion.Height;
            SidePanel.Top = menu_deconnexion.Top;

            this.Hide();

            Loginform Loginform = new Loginform();
            Loginform.ShowDialog();

            Application.Exit();
        }

        // function active menu
        private void ActivateButton(Button clickedButton)
        {
            menu_list_bapteme.BackColor = Color.FromArgb(41, 39, 41);
            menu_list_user.BackColor = Color.FromArgb(41, 39, 41);  
            menu_deconnexion.BackColor = Color.FromArgb(41, 39, 41);
            menu_welcome.BackColor = Color.FromArgb(41, 39, 41);
            menu_list_communion.BackColor = Color.FromArgb(41, 39, 41);
            menu_list_confirmation.BackColor = Color.FromArgb(41, 39, 41);
            menu_personne.BackColor = Color.FromArgb(41, 39, 41);

            // Change if button has clicked actif
            clickedButton.BackColor = Color.FromArgb(59, 57, 59);
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_reduce_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        public void load_form(object Form)
        {
            if (this.mainPanel.Controls.Count > 0)
            {
                this.mainPanel.Controls.RemoveAt(0);
            }

            Form f = Form as Form;
            f.TopLevel = false;
            f.Dock = DockStyle.Fill;
            this.mainPanel.Controls.Add(f);
            this.mainPanel.Tag = f;
            f.Show();
        }
    }
}
