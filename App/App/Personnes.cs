﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Loginform;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace App
{
    public partial class Personnes : Form
    {
        private Timer timer;
        private bool keyWasPressed = false;

        public Personnes()
        {
            InitializeComponent();

            timer = new Timer();
            timer.Interval = 800;
            timer.Tick += Timer_Tick;

            textRecherche.KeyUp += TextRecherche_Key_up;
        }

        private void OpenFormPersonne()
        {
            AjoutPersonne ajoutPersonne = new AjoutPersonne(this);
            ajoutPersonne.Show();

            ModifierPersonne modifierPersonne = new ModifierPersonne(this);
            ajoutPersonne.Show();
        }

        public static class EditPersonneid
        {
            public static int Id { get; set; }
        }

        private void button_ajouter_Click(object sender, EventArgs e)
        {
            AjoutPersonne AjoutPersonne = new AjoutPersonne(this);
            AjoutPersonne.ShowDialog(this);
        }

        private void Personnes_Load(object sender, EventArgs e)
        {
            show_data_personne();

            showhide_button_action();

            textRecherche.Text = "Recherche par nom ou prénom de la personne";
            textRecherche.ForeColor = Color.LightGray;
            textRecherche.BackColor = Color.White;
        }

        public void show_data_personne()
        {
            table_personne.Rows.Clear();
            table_personne.ReadOnly = true;

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT * FROM personne";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        while (readerList.Read())
                        {
                            object[] rowData = new object[readerList.FieldCount];
                            readerList.GetValues(rowData);

                            for (int i = 0; i < rowData.Length; i++)
                            {
                                if (rowData[i] is DateTime)
                                {
                                    rowData[i] = ((DateTime)rowData[i]).ToString("dd/MM/yyyy");
                                }
                            }

                            table_personne.Rows.Add(rowData);
                            table_personne.ClearSelection();
                        }
                    }
                }

                int nombreResultats = table_personne.Rows.Count;
                labelResultat.Text = "Résultat(s) : " + nombreResultats + " enregistré(s)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void showhide_button_action()
        {
            string Profil = Session.Profil;

            if (Profil == "Administrateur")
            {
                button_ajouter.Visible = true;
                button_modifier.Visible = true;
                button_supprimer.Visible = true;
            }
            else
            {
                button_ajouter.Visible = false;
                button_modifier.Visible = false;
                button_supprimer.Visible = false;
                label_alert_user.Visible = true;
            }
        }

        private void table_personne_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                DataGridViewRow selectedRow = table_personne.Rows[e.RowIndex];
                selectedRow.Selected = true;
            }
        }

        private string GetSelectedRowID()
        {
            if (table_personne.SelectedRows.Count > 0)
            {
                string selectedID = table_personne.SelectedRows[0].Cells[0].Value.ToString();
                return selectedID;
            }
            else
            {
                return null;
            }
        }

        private void button_modifier_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                EditPersonneid.Id = Convert.ToInt32(selectedID);

                ModifierPersonne modifierPersonne = new ModifierPersonne(this);
                modifierPersonne.ShowDialog(this);
            }
        }

        private void textRecherche_Leave(object sender, EventArgs e)
        {
            if (textRecherche.Text == "")
            {
                textRecherche.Text = "Recherche par nom ou prénom de la personne";
                textRecherche.ForeColor = Color.LightGray;
                textRecherche.BackColor = Color.White;
            }
        }

        private void textRecherche_Enter(object sender, EventArgs e)
        {
            if (textRecherche.Text == "Recherche par nom ou prénom de la personne")
            {
                textRecherche.Text = "";
                textRecherche.ForeColor = Color.Black;
                textRecherche.BackColor = Color.WhiteSmoke;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            keyWasPressed = false;
            string recherche = textRecherche.Text;

            if (!string.IsNullOrEmpty(recherche))
            {
                recherchePersonne(recherche);
            }
            else
            {
                show_data_personne();
            }
        }

        private void TextRecherche_Key_up(object sender, KeyEventArgs e)
        {
            keyWasPressed = true;
            timer.Stop();
            timer.Start();
        }

        private void recherchePersonne (string recherche)
        {
            table_personne.Rows.Clear();
            table_personne.ReadOnly = true;

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT * FROM personne WHERE last_name LIKE @lastnameStartsWith OR first_name LIKE @firstnameStartsWith";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@lastnameStartsWith", "%" + recherche + "%");
                    command.Parameters.AddWithValue("@firstnameStartsWith", "%" + recherche + "%");

                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        while (readerList.Read())
                        {
                            object[] rowData = new object[readerList.FieldCount];
                            readerList.GetValues(rowData);

                            for (int i = 0; i < rowData.Length; i++)
                            {
                                if (rowData[i] is DateTime)
                                {
                                    rowData[i] = ((DateTime)rowData[i]).ToString("dd/MM/yyyy");
                                }
                            }

                            table_personne.Rows.Add(rowData);
                            table_personne.ClearSelection();
                        }
                    }
                }

                int nombreResultats = table_personne.Rows.Count;
                labelResultat.Text = "Résultat(s) : " + nombreResultats + " enregistré(s)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_supprimer_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DialogResult result = MessageBox.Show("Voulez-vous vraiment supprimer cette personne ?", "Confirmation de suppression", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    try
                    {
                        string deleteQuery = "DELETE FROM personne WHERE id_personne = @id";

                        using (SQLiteCommand deleteCommand = new SQLiteCommand(deleteQuery, connection))
                        {
                            deleteCommand.Parameters.AddWithValue("@id", selectedID);

                            int rowsAffected = deleteCommand.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                MessageBox.Show("La personne a été supprimé avec succès!", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                deleteAllLineOfthisPersonne(selectedID);

                                show_data_personne();
                            }
                            else
                            {
                                MessageBox.Show("Échec de la suppression de la personne.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erreur lors de la suppression de la personne: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void deleteAllLineOfthisPersonne(string selectedID)
        {
            string deleteQueryBapteme = "DELETE FROM bapteme WHERE id_personne = @id";
            string deleteQueryCommunion = "DELETE FROM communion WHERE id_personne = @id";
            string deleteQueryConfirmation = "DELETE FROM confirmation WHERE id_personne = @id";

            deleteEvent(deleteQueryBapteme, selectedID);
            deleteEvent(deleteQueryCommunion, selectedID);
            deleteEvent(deleteQueryConfirmation, selectedID);
        }

        private void deleteEvent(string query, string selectedID) 
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                using (SQLiteCommand deleteCommand = new SQLiteCommand(query, connection))
                {
                    deleteCommand.Parameters.AddWithValue("@id", selectedID);

                    int rowsAffected = deleteCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur lors de la suppression de la personne: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                EditPersonneid.Id = Convert.ToInt32(selectedID);

                DetailsPersonne detailsPersonne = new DetailsPersonne();
                detailsPersonne.ShowDialog();
            }
        }
    }
}
