﻿namespace App
{
    partial class AjoutUtilisateur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AjoutUtilisateur));
            this.panel_moved = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button_exit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.profil_select = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textFirstName = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textLastName = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button_retour = new System.Windows.Forms.Button();
            this.textPasswordConfirm = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.textPasswordCreate = new System.Windows.Forms.TextBox();
            this.textLoginUser = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button_enregistrer = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_moved.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_moved
            // 
            this.panel_moved.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.panel_moved.Controls.Add(this.label2);
            this.panel_moved.Controls.Add(this.button_exit);
            this.panel_moved.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_moved.Location = new System.Drawing.Point(0, 0);
            this.panel_moved.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel_moved.Name = "panel_moved";
            this.panel_moved.Size = new System.Drawing.Size(368, 32);
            this.panel_moved.TabIndex = 0;
            this.panel_moved.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseDown);
            this.panel_moved.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseMove);
            this.panel_moved.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 19);
            this.label2.TabIndex = 78;
            this.label2.Text = "Ajout d\'un utilisateur";
            // 
            // button_exit
            // 
            this.button_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            this.button_exit.FlatAppearance.BorderSize = 0;
            this.button_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_exit.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_exit.ForeColor = System.Drawing.Color.White;
            this.button_exit.Image = ((System.Drawing.Image)(resources.GetObject("button_exit.Image")));
            this.button_exit.Location = new System.Drawing.Point(337, 0);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(31, 32);
            this.button_exit.TabIndex = 78;
            this.button_exit.UseVisualStyleBackColor = false;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.profil_select);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.textFirstName);
            this.panel2.Controls.Add(this.pictureBox6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.textLastName);
            this.panel2.Controls.Add(this.pictureBox5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.button_retour);
            this.panel2.Controls.Add(this.textPasswordConfirm);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Controls.Add(this.textPasswordCreate);
            this.panel2.Controls.Add(this.textLoginUser);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.button_enregistrer);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(9, 40);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(350, 522);
            this.panel2.TabIndex = 1;
            // 
            // profil_select
            // 
            this.profil_select.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.profil_select.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profil_select.ForeColor = System.Drawing.SystemColors.ControlText;
            this.profil_select.Items.AddRange(new object[] {
            "Administrateur",
            "Utilisateur"});
            this.profil_select.Location = new System.Drawing.Point(75, 275);
            this.profil_select.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.profil_select.Name = "profil_select";
            this.profil_select.Size = new System.Drawing.Size(239, 25);
            this.profil_select.TabIndex = 77;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(36, 273);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 76;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel6.Location = new System.Drawing.Point(36, 305);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(279, 1);
            this.panel6.TabIndex = 75;
            // 
            // textFirstName
            // 
            this.textFirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textFirstName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFirstName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textFirstName.Location = new System.Drawing.Point(75, 85);
            this.textFirstName.Name = "textFirstName";
            this.textFirstName.Size = new System.Drawing.Size(238, 19);
            this.textFirstName.TabIndex = 74;
            this.textFirstName.Enter += new System.EventHandler(this.textFirstName_Enter);
            this.textFirstName.Leave += new System.EventHandler(this.textFirstName_Leave);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(34, 82);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(28, 27);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 73;
            this.pictureBox6.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel5.Location = new System.Drawing.Point(34, 114);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(279, 1);
            this.panel5.TabIndex = 72;
            // 
            // textLastName
            // 
            this.textLastName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textLastName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLastName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textLastName.Location = new System.Drawing.Point(75, 150);
            this.textLastName.Name = "textLastName";
            this.textLastName.Size = new System.Drawing.Size(238, 19);
            this.textLastName.TabIndex = 71;
            this.textLastName.Enter += new System.EventHandler(this.textLastName_Enter);
            this.textLastName.Leave += new System.EventHandler(this.textLastName_Leave);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(34, 146);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(28, 27);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 70;
            this.pictureBox5.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel4.Location = new System.Drawing.Point(34, 178);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(279, 1);
            this.panel4.TabIndex = 69;
            // 
            // button_retour
            // 
            this.button_retour.BackColor = System.Drawing.Color.Firebrick;
            this.button_retour.FlatAppearance.BorderSize = 0;
            this.button_retour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_retour.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_retour.ForeColor = System.Drawing.Color.White;
            this.button_retour.Location = new System.Drawing.Point(35, 465);
            this.button_retour.Name = "button_retour";
            this.button_retour.Size = new System.Drawing.Size(117, 41);
            this.button_retour.TabIndex = 68;
            this.button_retour.Text = "Retour";
            this.button_retour.UseVisualStyleBackColor = false;
            this.button_retour.Click += new System.EventHandler(this.button_retour_Click);
            // 
            // textPasswordConfirm
            // 
            this.textPasswordConfirm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPasswordConfirm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPasswordConfirm.ForeColor = System.Drawing.SystemColors.Desktop;
            this.textPasswordConfirm.Location = new System.Drawing.Point(75, 408);
            this.textPasswordConfirm.Name = "textPasswordConfirm";
            this.textPasswordConfirm.Size = new System.Drawing.Size(238, 19);
            this.textPasswordConfirm.TabIndex = 67;
            this.textPasswordConfirm.Enter += new System.EventHandler(this.textPasswordConfirm_Enter);
            this.textPasswordConfirm.Leave += new System.EventHandler(this.textPasswordConfirm_Leave);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel3.Location = new System.Drawing.Point(35, 434);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(279, 1);
            this.panel3.TabIndex = 65;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(35, 405);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(28, 24);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 66;
            this.pictureBox4.TabStop = false;
            // 
            // textPasswordCreate
            // 
            this.textPasswordCreate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPasswordCreate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPasswordCreate.ForeColor = System.Drawing.SystemColors.Desktop;
            this.textPasswordCreate.Location = new System.Drawing.Point(75, 348);
            this.textPasswordCreate.Name = "textPasswordCreate";
            this.textPasswordCreate.Size = new System.Drawing.Size(238, 19);
            this.textPasswordCreate.TabIndex = 64;
            this.textPasswordCreate.Enter += new System.EventHandler(this.textPasswordCreate_Enter);
            this.textPasswordCreate.Leave += new System.EventHandler(this.textPasswordCreate_Leave);
            // 
            // textLoginUser
            // 
            this.textLoginUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textLoginUser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textLoginUser.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textLoginUser.Location = new System.Drawing.Point(75, 213);
            this.textLoginUser.Name = "textLoginUser";
            this.textLoginUser.Size = new System.Drawing.Size(238, 19);
            this.textLoginUser.TabIndex = 63;
            this.textLoginUser.Enter += new System.EventHandler(this.textLoginUser_Enter);
            this.textLoginUser.Leave += new System.EventHandler(this.textLoginUser_Leave);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel7.Location = new System.Drawing.Point(35, 374);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(279, 1);
            this.panel7.TabIndex = 59;
            // 
            // button_enregistrer
            // 
            this.button_enregistrer.BackColor = System.Drawing.SystemColors.Highlight;
            this.button_enregistrer.FlatAppearance.BorderSize = 0;
            this.button_enregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_enregistrer.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_enregistrer.ForeColor = System.Drawing.Color.White;
            this.button_enregistrer.Location = new System.Drawing.Point(197, 465);
            this.button_enregistrer.Name = "button_enregistrer";
            this.button_enregistrer.Size = new System.Drawing.Size(117, 41);
            this.button_enregistrer.TabIndex = 62;
            this.button_enregistrer.Text = "Enregistrer";
            this.button_enregistrer.UseVisualStyleBackColor = false;
            this.button_enregistrer.Click += new System.EventHandler(this.button_enregistrer_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(34, 339);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(29, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 61;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(34, 209);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(28, 27);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 60;
            this.pictureBox2.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel8.Location = new System.Drawing.Point(34, 240);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(279, 1);
            this.panel8.TabIndex = 58;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(40, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 29);
            this.label1.TabIndex = 57;
            this.label1.Text = "Veuillez remplir les champs :";
            // 
            // AjoutUtilisateur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(368, 570);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel_moved);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "AjoutUtilisateur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AjoutUtilisateurcs";
            this.Load += new System.EventHandler(this.AjoutUtilisateur_Load);
            this.panel_moved.ResumeLayout(false);
            this.panel_moved.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_moved;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textFirstName;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textLastName;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button_retour;
        private System.Windows.Forms.TextBox textPasswordConfirm;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox textPasswordCreate;
        private System.Windows.Forms.TextBox textLoginUser;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button_enregistrer;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox profil_select;
    }
}