﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace App
{
    public partial class AjoutBapteme : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        private Bapteme _formBaptemeReference;

        public AjoutBapteme(Bapteme formBaptemeReference)
        {
            InitializeComponent();

            _formBaptemeReference = formBaptemeReference;
        }

        private void AjourBapteme_Load(object sender, EventArgs e)
        {
            textPriestName.Text = "Entrez le nom du prêtre";
            textPriestName.ForeColor = Color.LightGray;
            textGodFather.Text = "Entrez le nom du parrain ou marraine";
            textGodFather.ForeColor = Color.LightGray;

            getAllPersonneNoBapteme();
        }

        private void getAllPersonneNoBapteme()
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT p.id_personne, p.last_name, p.first_name FROM personne AS p " +
               "LEFT JOIN bapteme AS b ON b.id_personne = p.id_personne " +
               "WHERE b.id_personne IS NULL ORDER BY p.last_name, p.first_name ASC";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        select_personne.Items.Clear();
                        select_personne.DropDownStyle = ComboBoxStyle.DropDownList;

                        List<Person> people = new List<Person>();

                        while (readerList.Read())
                        {
                            string fullName = $"{readerList["last_name"]} {readerList["first_name"]}";
                            int idPersonne = Convert.ToInt32(readerList["id_personne"]);

                            people.Add(new Person(idPersonne, fullName));
                        }

                        select_personne.DataSource = people;
                        select_personne.DisplayMember = "FullName";
                        select_personne.ValueMember = "Id";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void textPriestName_Enter(object sender, EventArgs e)
        {
            if (textPriestName.Text == "Entrez le nom du prêtre")
            {
                textPriestName.Text = "";
                textPriestName.ForeColor = Color.Black;
            }
        }

        private void textPriestName_Leave(object sender, EventArgs e)
        {
            if (textPriestName.Text == "")
            {
                textPriestName.Text = "Entrez le nom du prêtre";
                textPriestName.ForeColor = Color.LightGray;
            }
        }

        private void textGodFather_Enter(object sender, EventArgs e)
        {
            if (textGodFather.Text == "Entrez le nom du parrain ou marraine")
            {
                textGodFather.Text = "";
                textGodFather.ForeColor = Color.Black;
            }
        }

        private void textGodFather_Leave(object sender, EventArgs e)
        {
            if (textGodFather.Text == "")
            {
                textGodFather.Text = "Entrez le nom du parrain ou marraine";
                textGodFather.ForeColor = Color.LightGray;
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            string priest = textPriestName.Text;
            string godfather = textGodFather.Text;
            DateTime date_bapteme = dateBapteme.Value.Date;
            int id_personne ;

            if (select_personne != null && select_personne.SelectedItem != null)
            {
               id_personne = ((Person)select_personne.SelectedItem).Id;
            }
            else
            {
                MessageBox.Show("La liste déroulante est vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(priest) || priest == "Entrez le nom du prêtre")
            {
                MessageBox.Show("Le champs nom du prêtre ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(godfather) || godfather == "Entrez le nom du parrain ou marraine")
            {
                MessageBox.Show("Le champs nom du parrain ou de la marraine ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (date_bapteme == null || dateBapteme.Value == DateTime.MinValue)
            {
                MessageBox.Show("Veuillez sélectionner une date de baptême.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string insertQuery = "INSERT INTO bapteme (priest, godfather, date_bapteme, id_personne) " +
                                        "VALUES (@priest, @godfather, @date_bapteme, @id_personne)";

                using (SQLiteCommand insertCommand = new SQLiteCommand(insertQuery, connection))
                {
                    insertCommand.Parameters.AddWithValue("@priest", priest);
                    insertCommand.Parameters.AddWithValue("@godfather", godfather);
                    insertCommand.Parameters.AddWithValue("@date_bapteme", date_bapteme);
                    insertCommand.Parameters.AddWithValue("@id_personne", id_personne);

                    int rowsAffected = insertCommand.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Les informations sur le baptême ont été insérées avec succès !", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Hide();

                        _formBaptemeReference.show_data_bapteme();
                    }
                    else
                    {
                        MessageBox.Show("Erreur lors de l'insertion des informations du baptême", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
