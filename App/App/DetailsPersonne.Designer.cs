﻿namespace App
{
    partial class DetailsPersonne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetailsPersonne));
            this.button_exit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelInfoPersonne = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelDateConfirmation = new System.Windows.Forms.Label();
            this.labelNomParrainConfirmation = new System.Windows.Forms.Label();
            this.labelNomPretreConfirmation = new System.Windows.Forms.Label();
            this.labelDateCommunion = new System.Windows.Forms.Label();
            this.labelNomPretreCommunion = new System.Windows.Forms.Label();
            this.labelDateBapteme = new System.Windows.Forms.Label();
            this.labelNomParrainBapteme = new System.Windows.Forms.Label();
            this.labelNomPrereBapteme = new System.Windows.Forms.Label();
            this.labelAdresse = new System.Windows.Forms.Label();
            this.labelNomMere = new System.Windows.Forms.Label();
            this.labelNumeroPere = new System.Windows.Forms.Label();
            this.labelSexe = new System.Windows.Forms.Label();
            this.labelNomPere = new System.Windows.Forms.Label();
            this.labelLieuNaissance = new System.Windows.Forms.Label();
            this.labelDateNaissance = new System.Windows.Forms.Label();
            this.labelPrenom = new System.Windows.Forms.Label();
            this.labelNom = new System.Windows.Forms.Label();
            this.button_retour = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelInfoConfirmation = new System.Windows.Forms.Label();
            this.labelInfoCommunion = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelInfoBapteme = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel_moved = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.labelNumeroMere = new System.Windows.Forms.Label();
            this.buttonPDF = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel_moved.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_exit
            // 
            this.button_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            this.button_exit.FlatAppearance.BorderSize = 0;
            this.button_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_exit.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_exit.ForeColor = System.Drawing.Color.White;
            this.button_exit.Image = ((System.Drawing.Image)(resources.GetObject("button_exit.Image")));
            this.button_exit.Location = new System.Drawing.Point(631, 0);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(31, 32);
            this.button_exit.TabIndex = 78;
            this.button_exit.UseVisualStyleBackColor = false;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(272, 19);
            this.label2.TabIndex = 78;
            this.label2.Text = "Détails des information de la Personne";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 19);
            this.label3.TabIndex = 90;
            this.label3.Text = "Nom :";
            // 
            // labelInfoPersonne
            // 
            this.labelInfoPersonne.AutoSize = true;
            this.labelInfoPersonne.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoPersonne.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelInfoPersonne.Location = new System.Drawing.Point(25, 9);
            this.labelInfoPersonne.Name = "labelInfoPersonne";
            this.labelInfoPersonne.Size = new System.Drawing.Size(262, 26);
            this.labelInfoPersonne.TabIndex = 57;
            this.labelInfoPersonne.Text = "Informations de la Personne :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.buttonPDF);
            this.panel2.Controls.Add(this.labelNumeroMere);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.labelDateConfirmation);
            this.panel2.Controls.Add(this.labelNomParrainConfirmation);
            this.panel2.Controls.Add(this.labelNomPretreConfirmation);
            this.panel2.Controls.Add(this.labelDateCommunion);
            this.panel2.Controls.Add(this.labelNomPretreCommunion);
            this.panel2.Controls.Add(this.labelDateBapteme);
            this.panel2.Controls.Add(this.labelNomParrainBapteme);
            this.panel2.Controls.Add(this.labelNomPrereBapteme);
            this.panel2.Controls.Add(this.labelAdresse);
            this.panel2.Controls.Add(this.labelNomMere);
            this.panel2.Controls.Add(this.labelNumeroPere);
            this.panel2.Controls.Add(this.labelSexe);
            this.panel2.Controls.Add(this.labelNomPere);
            this.panel2.Controls.Add(this.labelLieuNaissance);
            this.panel2.Controls.Add(this.labelDateNaissance);
            this.panel2.Controls.Add(this.labelPrenom);
            this.panel2.Controls.Add(this.labelNom);
            this.panel2.Controls.Add(this.button_retour);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.labelInfoConfirmation);
            this.panel2.Controls.Add(this.labelInfoCommunion);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.labelInfoBapteme);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.labelInfoPersonne);
            this.panel2.Location = new System.Drawing.Point(6, 37);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(650, 794);
            this.panel2.TabIndex = 7;
            // 
            // labelDateConfirmation
            // 
            this.labelDateConfirmation.AutoSize = true;
            this.labelDateConfirmation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateConfirmation.Location = new System.Drawing.Point(201, 725);
            this.labelDateConfirmation.Name = "labelDateConfirmation";
            this.labelDateConfirmation.Size = new System.Drawing.Size(169, 19);
            this.labelDateConfirmation.TabIndex = 137;
            this.labelDateConfirmation.Text = "Date de la confirmation :";
            // 
            // labelNomParrainConfirmation
            // 
            this.labelNomParrainConfirmation.AutoSize = true;
            this.labelNomParrainConfirmation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomParrainConfirmation.Location = new System.Drawing.Point(267, 689);
            this.labelNomParrainConfirmation.Name = "labelNomParrainConfirmation";
            this.labelNomParrainConfirmation.Size = new System.Drawing.Size(235, 19);
            this.labelNomParrainConfirmation.TabIndex = 136;
            this.labelNomParrainConfirmation.Text = "Nom du parrain ou de la marraine :";
            // 
            // labelNomPretreConfirmation
            // 
            this.labelNomPretreConfirmation.AutoSize = true;
            this.labelNomPretreConfirmation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomPretreConfirmation.Location = new System.Drawing.Point(142, 651);
            this.labelNomPretreConfirmation.Name = "labelNomPretreConfirmation";
            this.labelNomPretreConfirmation.Size = new System.Drawing.Size(110, 19);
            this.labelNomPretreConfirmation.TabIndex = 135;
            this.labelNomPretreConfirmation.Text = "Nom du prêtre :";
            // 
            // labelDateCommunion
            // 
            this.labelDateCommunion.AutoSize = true;
            this.labelDateCommunion.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateCommunion.Location = new System.Drawing.Point(195, 575);
            this.labelDateCommunion.Name = "labelDateCommunion";
            this.labelDateCommunion.Size = new System.Drawing.Size(163, 19);
            this.labelDateCommunion.TabIndex = 134;
            this.labelDateCommunion.Text = "Date de la communion :";
            // 
            // labelNomPretreCommunion
            // 
            this.labelNomPretreCommunion.AutoSize = true;
            this.labelNomPretreCommunion.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomPretreCommunion.Location = new System.Drawing.Point(142, 540);
            this.labelNomPretreCommunion.Name = "labelNomPretreCommunion";
            this.labelNomPretreCommunion.Size = new System.Drawing.Size(110, 19);
            this.labelNomPretreCommunion.TabIndex = 133;
            this.labelNomPretreCommunion.Text = "Nom du prêtre :";
            // 
            // labelDateBapteme
            // 
            this.labelDateBapteme.AutoSize = true;
            this.labelDateBapteme.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateBapteme.Location = new System.Drawing.Point(178, 471);
            this.labelDateBapteme.Name = "labelDateBapteme";
            this.labelDateBapteme.Size = new System.Drawing.Size(145, 19);
            this.labelDateBapteme.TabIndex = 132;
            this.labelDateBapteme.Text = "Date de la baptême :";
            // 
            // labelNomParrainBapteme
            // 
            this.labelNomParrainBapteme.AutoSize = true;
            this.labelNomParrainBapteme.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomParrainBapteme.Location = new System.Drawing.Point(267, 436);
            this.labelNomParrainBapteme.Name = "labelNomParrainBapteme";
            this.labelNomParrainBapteme.Size = new System.Drawing.Size(235, 19);
            this.labelNomParrainBapteme.TabIndex = 131;
            this.labelNomParrainBapteme.Text = "Nom du parrain ou de la marraine :";
            // 
            // labelNomPrereBapteme
            // 
            this.labelNomPrereBapteme.AutoSize = true;
            this.labelNomPrereBapteme.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomPrereBapteme.Location = new System.Drawing.Point(142, 404);
            this.labelNomPrereBapteme.Name = "labelNomPrereBapteme";
            this.labelNomPrereBapteme.Size = new System.Drawing.Size(110, 19);
            this.labelNomPrereBapteme.TabIndex = 130;
            this.labelNomPrereBapteme.Text = "Nom du prêtre :";
            // 
            // labelAdresse
            // 
            this.labelAdresse.AutoSize = true;
            this.labelAdresse.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdresse.Location = new System.Drawing.Point(105, 339);
            this.labelAdresse.Name = "labelAdresse";
            this.labelAdresse.Size = new System.Drawing.Size(73, 19);
            this.labelAdresse.TabIndex = 129;
            this.labelAdresse.Text = "Adresse  :";
            // 
            // labelNomMere
            // 
            this.labelNomMere.AutoSize = true;
            this.labelNomMere.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomMere.Location = new System.Drawing.Point(152, 274);
            this.labelNomMere.Name = "labelNomMere";
            this.labelNomMere.Size = new System.Drawing.Size(120, 19);
            this.labelNomMere.TabIndex = 128;
            this.labelNomMere.Text = "Nom de la mère :";
            // 
            // labelNumeroPere
            // 
            this.labelNumeroPere.AutoSize = true;
            this.labelNumeroPere.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumeroPere.Location = new System.Drawing.Point(174, 240);
            this.labelNumeroPere.Name = "labelNumeroPere";
            this.labelNumeroPere.Size = new System.Drawing.Size(143, 19);
            this.labelNumeroPere.TabIndex = 127;
            this.labelNumeroPere.Text = "Numéro Téléphone  :";
            // 
            // labelSexe
            // 
            this.labelSexe.AutoSize = true;
            this.labelSexe.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSexe.Location = new System.Drawing.Point(79, 105);
            this.labelSexe.Name = "labelSexe";
            this.labelSexe.Size = new System.Drawing.Size(47, 19);
            this.labelSexe.TabIndex = 126;
            this.labelSexe.Text = "sexe :";
            // 
            // labelNomPere
            // 
            this.labelNomPere.AutoSize = true;
            this.labelNomPere.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomPere.Location = new System.Drawing.Point(132, 203);
            this.labelNomPere.Name = "labelNomPere";
            this.labelNomPere.Size = new System.Drawing.Size(100, 19);
            this.labelNomPere.TabIndex = 125;
            this.labelNomPere.Text = "Nom du père :";
            // 
            // labelLieuNaissance
            // 
            this.labelLieuNaissance.AutoSize = true;
            this.labelLieuNaissance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLieuNaissance.Location = new System.Drawing.Point(161, 171);
            this.labelLieuNaissance.Name = "labelLieuNaissance";
            this.labelLieuNaissance.Size = new System.Drawing.Size(133, 19);
            this.labelLieuNaissance.TabIndex = 124;
            this.labelLieuNaissance.Text = "Lieu de naissance :";
            // 
            // labelDateNaissance
            // 
            this.labelDateNaissance.AutoSize = true;
            this.labelDateNaissance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateNaissance.Location = new System.Drawing.Point(169, 134);
            this.labelDateNaissance.Name = "labelDateNaissance";
            this.labelDateNaissance.Size = new System.Drawing.Size(137, 19);
            this.labelDateNaissance.TabIndex = 123;
            this.labelDateNaissance.Text = "Date de naissance :";
            // 
            // labelPrenom
            // 
            this.labelPrenom.AutoSize = true;
            this.labelPrenom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrenom.Location = new System.Drawing.Point(98, 75);
            this.labelPrenom.Name = "labelPrenom";
            this.labelPrenom.Size = new System.Drawing.Size(66, 19);
            this.labelPrenom.TabIndex = 122;
            this.labelPrenom.Text = "Prénom :";
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNom.Location = new System.Drawing.Point(79, 46);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(47, 19);
            this.labelNom.TabIndex = 121;
            this.labelNom.Text = "Nom :";
            // 
            // button_retour
            // 
            this.button_retour.BackColor = System.Drawing.Color.Firebrick;
            this.button_retour.FlatAppearance.BorderSize = 0;
            this.button_retour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_retour.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_retour.ForeColor = System.Drawing.Color.White;
            this.button_retour.Location = new System.Drawing.Point(518, 756);
            this.button_retour.Name = "button_retour";
            this.button_retour.Size = new System.Drawing.Size(117, 32);
            this.button_retour.TabIndex = 120;
            this.button_retour.Text = "Fermer";
            this.button_retour.UseVisualStyleBackColor = false;
            this.button_retour.Click += new System.EventHandler(this.button_retour_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(26, 725);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(169, 19);
            this.label20.TabIndex = 119;
            this.label20.Text = "Date de la confirmation :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(26, 689);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(235, 19);
            this.label22.TabIndex = 117;
            this.label22.Text = "Nom du parrain ou de la marraine :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(26, 651);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 19);
            this.label23.TabIndex = 116;
            this.label23.Text = "Nom du prêtre :";
            // 
            // labelInfoConfirmation
            // 
            this.labelInfoConfirmation.AutoSize = true;
            this.labelInfoConfirmation.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoConfirmation.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelInfoConfirmation.Location = new System.Drawing.Point(25, 611);
            this.labelInfoConfirmation.Name = "labelInfoConfirmation";
            this.labelInfoConfirmation.Size = new System.Drawing.Size(305, 26);
            this.labelInfoConfirmation.TabIndex = 115;
            this.labelInfoConfirmation.Text = "Informations  sur la Confirmation :";
            // 
            // labelInfoCommunion
            // 
            this.labelInfoCommunion.AutoSize = true;
            this.labelInfoCommunion.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoCommunion.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelInfoCommunion.Location = new System.Drawing.Point(25, 501);
            this.labelInfoCommunion.Name = "labelInfoCommunion";
            this.labelInfoCommunion.Size = new System.Drawing.Size(298, 26);
            this.labelInfoCommunion.TabIndex = 114;
            this.labelInfoCommunion.Text = "Informations  sur la Communion :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(26, 575);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(163, 19);
            this.label14.TabIndex = 113;
            this.label14.Text = "Date de la communion :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(26, 540);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(110, 19);
            this.label18.TabIndex = 111;
            this.label18.Text = "Nom du prêtre :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(26, 471);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 19);
            this.label13.TabIndex = 110;
            this.label13.Text = "Date de la baptême :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(26, 436);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(235, 19);
            this.label15.TabIndex = 109;
            this.label15.Text = "Nom du parrain ou de la marraine :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(26, 404);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 19);
            this.label16.TabIndex = 108;
            this.label16.Text = "Nom du prêtre :";
            // 
            // labelInfoBapteme
            // 
            this.labelInfoBapteme.AutoSize = true;
            this.labelInfoBapteme.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfoBapteme.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelInfoBapteme.Location = new System.Drawing.Point(25, 365);
            this.labelInfoBapteme.Name = "labelInfoBapteme";
            this.labelInfoBapteme.Size = new System.Drawing.Size(269, 26);
            this.labelInfoBapteme.TabIndex = 107;
            this.labelInfoBapteme.Text = "Informations  sur le Baptême :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(26, 339);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 19);
            this.label11.TabIndex = 98;
            this.label11.Text = "Adresse  :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(26, 274);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 19);
            this.label10.TabIndex = 97;
            this.label10.Text = "Nom de la mère :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(26, 240);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 19);
            this.label9.TabIndex = 96;
            this.label9.Text = "Numéro Téléphone  :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 19);
            this.label8.TabIndex = 95;
            this.label8.Text = "Sexe :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(26, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 19);
            this.label7.TabIndex = 94;
            this.label7.Text = "Nom du père :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(26, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 19);
            this.label6.TabIndex = 93;
            this.label6.Text = "Lieu de naissance :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 19);
            this.label5.TabIndex = 92;
            this.label5.Text = "Date de naissance :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 19);
            this.label4.TabIndex = 91;
            this.label4.Text = "Prénom :";
            // 
            // panel_moved
            // 
            this.panel_moved.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.panel_moved.Controls.Add(this.label2);
            this.panel_moved.Controls.Add(this.button_exit);
            this.panel_moved.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_moved.Location = new System.Drawing.Point(0, 0);
            this.panel_moved.Margin = new System.Windows.Forms.Padding(2);
            this.panel_moved.Name = "panel_moved";
            this.panel_moved.Size = new System.Drawing.Size(662, 32);
            this.panel_moved.TabIndex = 6;
            this.panel_moved.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseDown);
            this.panel_moved.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseMove);
            this.panel_moved.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseUp);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(26, 308);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(143, 19);
            this.label21.TabIndex = 138;
            this.label21.Text = "Numéro Téléphone  :";
            // 
            // labelNumeroMere
            // 
            this.labelNumeroMere.AutoSize = true;
            this.labelNumeroMere.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumeroMere.Location = new System.Drawing.Point(175, 308);
            this.labelNumeroMere.Name = "labelNumeroMere";
            this.labelNumeroMere.Size = new System.Drawing.Size(143, 19);
            this.labelNumeroMere.TabIndex = 139;
            this.labelNumeroMere.Text = "Numéro Téléphone  :";
            // 
            // buttonPDF
            // 
            this.buttonPDF.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonPDF.FlatAppearance.BorderSize = 0;
            this.buttonPDF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPDF.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPDF.ForeColor = System.Drawing.Color.White;
            this.buttonPDF.Location = new System.Drawing.Point(369, 756);
            this.buttonPDF.Name = "buttonPDF";
            this.buttonPDF.Size = new System.Drawing.Size(117, 32);
            this.buttonPDF.TabIndex = 140;
            this.buttonPDF.Text = "PDF";
            this.buttonPDF.UseVisualStyleBackColor = false;
            this.buttonPDF.Click += new System.EventHandler(this.PDF_Click);
            // 
            // DetailsPersonne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(662, 837);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel_moved);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DetailsPersonne";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ApercuPersonne";
            this.Load += new System.EventHandler(this.DetailsPersonne_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel_moved.ResumeLayout(false);
            this.panel_moved.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInfoPersonne;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel_moved;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelInfoBapteme;
        private System.Windows.Forms.Label labelInfoConfirmation;
        private System.Windows.Forms.Label labelInfoCommunion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button_retour;
        private System.Windows.Forms.Label labelDateConfirmation;
        private System.Windows.Forms.Label labelNomParrainConfirmation;
        private System.Windows.Forms.Label labelNomPretreConfirmation;
        private System.Windows.Forms.Label labelDateCommunion;
        private System.Windows.Forms.Label labelNomPretreCommunion;
        private System.Windows.Forms.Label labelDateBapteme;
        private System.Windows.Forms.Label labelNomParrainBapteme;
        private System.Windows.Forms.Label labelNomPrereBapteme;
        private System.Windows.Forms.Label labelAdresse;
        private System.Windows.Forms.Label labelNomMere;
        private System.Windows.Forms.Label labelNumeroPere;
        private System.Windows.Forms.Label labelSexe;
        private System.Windows.Forms.Label labelNomPere;
        private System.Windows.Forms.Label labelLieuNaissance;
        private System.Windows.Forms.Label labelDateNaissance;
        private System.Windows.Forms.Label labelPrenom;
        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Label labelNumeroMere;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button buttonPDF;
    }
}