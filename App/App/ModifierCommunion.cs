﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Communion;

namespace App
{
    public partial class ModifierCommunion : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        private Communion _formCommunionReference;

        public ModifierCommunion(Communion formCommunionReference)
        {
            InitializeComponent();

            _formCommunionReference = formCommunionReference;
        }

        private void textPriestName_Enter(object sender, EventArgs e)
        {
            if (textPriestName.Text == "Entrez le nom du prêtre")
            {
                textPriestName.Text = "";
                textPriestName.ForeColor = Color.Black;
            }
        }

        private void textPriestName_Leave(object sender, EventArgs e)
        {
            if (textPriestName.Text == "")
            {
                textPriestName.Text = "Entrez le nom du prêtre";
                textPriestName.ForeColor = Color.LightGray;
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void ModifierCommunion_Load(object sender, EventArgs e)
        {
            int id = EditCommunionId.Id;

            show_Communion_by_id(id);
        }

        private void show_Communion_by_id(int id)
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT p.id_personne, c.id_communion, p.last_name, p.first_name, c.date_communion, c.priest FROM communion AS c " +
                    "INNER JOIN personne AS p ON p.id_personne = c.id_personne " +
                    "WHERE c.id_communion = @id";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", id);

                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        select_personne.Items.Clear();
                        select_personne.DropDownStyle = ComboBoxStyle.DropDownList;

                        List<Person> people = new List<Person>();

                        if (reader.Read())
                        {
                            int id_personne = Convert.ToInt32(reader["id_personne"]);
                            string first_name = reader["first_name"].ToString();
                            string last_name = reader["last_name"].ToString();
                            string priest = reader["priest"].ToString();
                          
                            DateTime date_communion = Convert.ToDateTime(reader["date_communion"]);

                            string fullName = $"{last_name} {first_name}";
                            int idPersonne = Convert.ToInt32(id_personne);

                            people.Add(new Person(idPersonne, fullName));

                            select_personne.DataSource = people;
                            select_personne.DisplayMember = "FullName";
                            select_personne.ValueMember = "Id";

                            textPriestName.Text = priest;
                         
                            select_personne.DropDownStyle = ComboBoxStyle.DropDownList;
                            dateCommunion.Value = date_communion;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            int id = EditCommunionId.Id;
            string priest = textPriestName.Text;
            DateTime date_communion = dateCommunion.Value.Date;
            int id_personne;

            if (select_personne != null && select_personne.SelectedItem != null)
            {
                id_personne = ((Person)select_personne.SelectedItem).Id;
            }
            else
            {
                MessageBox.Show("La liste déroulante est vide.","Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(priest) || priest == "Entrez le nom du prêtre")
            {
                MessageBox.Show("Le champs nom du prêtre ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (date_communion == null || dateCommunion.Value == DateTime.MinValue)
            {
                MessageBox.Show("Veuillez sélectionner une date de communion.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string updateQuery = "UPDATE communion SET priest = @priest, date_communion = @date_communion," +
                    " id_personne = @id_personne WHERE id_communion = @id_communion"; ;

                using (SQLiteCommand updateCommand = new SQLiteCommand(updateQuery, connection))
                {
                    updateCommand.Parameters.AddWithValue("@priest", priest);
                    updateCommand.Parameters.AddWithValue("@date_communion", date_communion);
                    updateCommand.Parameters.AddWithValue("@id_personne", id_personne);
                    updateCommand.Parameters.AddWithValue("@id_communion", id);

                    int rowsAffected = updateCommand.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Les informations sur la communion ont été modifiée avec succès !", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Hide();

                        _formCommunionReference.show_data_communion();
                    }
                    else
                    {
                        MessageBox.Show("Erreur lors de la modification des informations de la communion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
