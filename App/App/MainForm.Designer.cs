﻿namespace App
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.SideBar = new System.Windows.Forms.Panel();
            this.menu_personne = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Menu_moved = new System.Windows.Forms.PictureBox();
            this.menu_welcome = new System.Windows.Forms.Button();
            this.menu_list_confirmation = new System.Windows.Forms.Button();
            this.menu_deconnexion = new System.Windows.Forms.Button();
            this.menu_list_communion = new System.Windows.Forms.Button();
            this.SidePanel = new System.Windows.Forms.FlowLayoutPanel();
            this.menu_list_bapteme = new System.Windows.Forms.Button();
            this.menu_list_user = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.panel_moved = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button_reduce = new System.Windows.Forms.Button();
            this.button_exit = new System.Windows.Forms.Button();
            this.timerSideBar = new System.Windows.Forms.Timer(this.components);
            this.SideBar.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Menu_moved)).BeginInit();
            this.panel_moved.SuspendLayout();
            this.SuspendLayout();
            // 
            // SideBar
            // 
            this.SideBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.SideBar.Controls.Add(this.menu_personne);
            this.SideBar.Controls.Add(this.panel1);
            this.SideBar.Controls.Add(this.menu_welcome);
            this.SideBar.Controls.Add(this.menu_list_confirmation);
            this.SideBar.Controls.Add(this.menu_deconnexion);
            this.SideBar.Controls.Add(this.menu_list_communion);
            this.SideBar.Controls.Add(this.SidePanel);
            this.SideBar.Controls.Add(this.menu_list_bapteme);
            this.SideBar.Controls.Add(this.menu_list_user);
            this.SideBar.Location = new System.Drawing.Point(0, 0);
            this.SideBar.MaximumSize = new System.Drawing.Size(210, 684);
            this.SideBar.MinimumSize = new System.Drawing.Size(63, 684);
            this.SideBar.Name = "SideBar";
            this.SideBar.Size = new System.Drawing.Size(210, 684);
            this.SideBar.TabIndex = 0;
            // 
            // menu_personne
            // 
            this.menu_personne.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.menu_personne.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menu_personne.FlatAppearance.BorderSize = 0;
            this.menu_personne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menu_personne.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu_personne.ForeColor = System.Drawing.Color.White;
            this.menu_personne.Image = ((System.Drawing.Image)(resources.GetObject("menu_personne.Image")));
            this.menu_personne.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_personne.Location = new System.Drawing.Point(13, 211);
            this.menu_personne.Name = "menu_personne";
            this.menu_personne.Size = new System.Drawing.Size(197, 54);
            this.menu_personne.TabIndex = 17;
            this.menu_personne.Text = "Personnes";
            this.menu_personne.UseVisualStyleBackColor = false;
            this.menu_personne.Click += new System.EventHandler(this.menu_personne_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Menu_moved);
            this.panel1.Location = new System.Drawing.Point(0, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 59);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(82, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 29);
            this.label1.TabIndex = 80;
            this.label1.Text = "Menu";
            // 
            // Menu_moved
            // 
            this.Menu_moved.Image = ((System.Drawing.Image)(resources.GetObject("Menu_moved.Image")));
            this.Menu_moved.Location = new System.Drawing.Point(3, 10);
            this.Menu_moved.Name = "Menu_moved";
            this.Menu_moved.Size = new System.Drawing.Size(58, 37);
            this.Menu_moved.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Menu_moved.TabIndex = 0;
            this.Menu_moved.TabStop = false;
            this.Menu_moved.Click += new System.EventHandler(this.Menu_moved_Click);
            // 
            // menu_welcome
            // 
            this.menu_welcome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.menu_welcome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menu_welcome.FlatAppearance.BorderSize = 0;
            this.menu_welcome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menu_welcome.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu_welcome.ForeColor = System.Drawing.Color.White;
            this.menu_welcome.Image = ((System.Drawing.Image)(resources.GetObject("menu_welcome.Image")));
            this.menu_welcome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_welcome.Location = new System.Drawing.Point(13, 158);
            this.menu_welcome.Name = "menu_welcome";
            this.menu_welcome.Size = new System.Drawing.Size(197, 54);
            this.menu_welcome.TabIndex = 13;
            this.menu_welcome.Text = "Accueil";
            this.menu_welcome.UseVisualStyleBackColor = false;
            this.menu_welcome.Click += new System.EventHandler(this.menu_welcome_Click);
            // 
            // menu_list_confirmation
            // 
            this.menu_list_confirmation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.menu_list_confirmation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menu_list_confirmation.FlatAppearance.BorderSize = 0;
            this.menu_list_confirmation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menu_list_confirmation.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu_list_confirmation.ForeColor = System.Drawing.Color.White;
            this.menu_list_confirmation.Image = ((System.Drawing.Image)(resources.GetObject("menu_list_confirmation.Image")));
            this.menu_list_confirmation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_list_confirmation.Location = new System.Drawing.Point(13, 372);
            this.menu_list_confirmation.Name = "menu_list_confirmation";
            this.menu_list_confirmation.Size = new System.Drawing.Size(197, 54);
            this.menu_list_confirmation.TabIndex = 16;
            this.menu_list_confirmation.Text = "  Confirmation";
            this.menu_list_confirmation.UseVisualStyleBackColor = false;
            this.menu_list_confirmation.Click += new System.EventHandler(this.menu_list_confirmation_Click);
            // 
            // menu_deconnexion
            // 
            this.menu_deconnexion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.menu_deconnexion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menu_deconnexion.FlatAppearance.BorderSize = 0;
            this.menu_deconnexion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menu_deconnexion.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu_deconnexion.ForeColor = System.Drawing.Color.White;
            this.menu_deconnexion.Image = ((System.Drawing.Image)(resources.GetObject("menu_deconnexion.Image")));
            this.menu_deconnexion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_deconnexion.Location = new System.Drawing.Point(13, 612);
            this.menu_deconnexion.Name = "menu_deconnexion";
            this.menu_deconnexion.Size = new System.Drawing.Size(197, 52);
            this.menu_deconnexion.TabIndex = 12;
            this.menu_deconnexion.Text = "         Se  déconnecter";
            this.menu_deconnexion.UseVisualStyleBackColor = false;
            this.menu_deconnexion.Click += new System.EventHandler(this.menu_deconnexion_Click);
            // 
            // menu_list_communion
            // 
            this.menu_list_communion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.menu_list_communion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menu_list_communion.FlatAppearance.BorderSize = 0;
            this.menu_list_communion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menu_list_communion.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu_list_communion.ForeColor = System.Drawing.Color.White;
            this.menu_list_communion.Image = ((System.Drawing.Image)(resources.GetObject("menu_list_communion.Image")));
            this.menu_list_communion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_list_communion.Location = new System.Drawing.Point(13, 318);
            this.menu_list_communion.Name = "menu_list_communion";
            this.menu_list_communion.Size = new System.Drawing.Size(197, 54);
            this.menu_list_communion.TabIndex = 15;
            this.menu_list_communion.Text = "  Communion";
            this.menu_list_communion.UseVisualStyleBackColor = false;
            this.menu_list_communion.Click += new System.EventHandler(this.menu_list_communion_Click);
            // 
            // SidePanel
            // 
            this.SidePanel.BackColor = System.Drawing.SystemColors.Highlight;
            this.SidePanel.Location = new System.Drawing.Point(-1, 158);
            this.SidePanel.Name = "SidePanel";
            this.SidePanel.Size = new System.Drawing.Size(11, 54);
            this.SidePanel.TabIndex = 11;
            // 
            // menu_list_bapteme
            // 
            this.menu_list_bapteme.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.menu_list_bapteme.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menu_list_bapteme.FlatAppearance.BorderSize = 0;
            this.menu_list_bapteme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menu_list_bapteme.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu_list_bapteme.ForeColor = System.Drawing.Color.White;
            this.menu_list_bapteme.Image = ((System.Drawing.Image)(resources.GetObject("menu_list_bapteme.Image")));
            this.menu_list_bapteme.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_list_bapteme.Location = new System.Drawing.Point(13, 265);
            this.menu_list_bapteme.Name = "menu_list_bapteme";
            this.menu_list_bapteme.Size = new System.Drawing.Size(197, 54);
            this.menu_list_bapteme.TabIndex = 9;
            this.menu_list_bapteme.Text = "  Baptême";
            this.menu_list_bapteme.UseVisualStyleBackColor = false;
            this.menu_list_bapteme.Click += new System.EventHandler(this.menu_list_bapteme_Click);
            // 
            // menu_list_user
            // 
            this.menu_list_user.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.menu_list_user.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menu_list_user.FlatAppearance.BorderSize = 0;
            this.menu_list_user.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.menu_list_user.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu_list_user.ForeColor = System.Drawing.Color.White;
            this.menu_list_user.Image = ((System.Drawing.Image)(resources.GetObject("menu_list_user.Image")));
            this.menu_list_user.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_list_user.Location = new System.Drawing.Point(13, 556);
            this.menu_list_user.Name = "menu_list_user";
            this.menu_list_user.Size = new System.Drawing.Size(197, 52);
            this.menu_list_user.TabIndex = 10;
            this.menu_list_user.Text = "    Utilisateurs";
            this.menu_list_user.UseVisualStyleBackColor = false;
            this.menu_list_user.Click += new System.EventHandler(this.menu_list_user_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.Gainsboro;
            this.mainPanel.Location = new System.Drawing.Point(217, 33);
            this.mainPanel.MaximumSize = new System.Drawing.Size(1070, 645);
            this.mainPanel.MinimumSize = new System.Drawing.Size(922, 645);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(922, 645);
            this.mainPanel.TabIndex = 2;
            // 
            // panel_moved
            // 
            this.panel_moved.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(41)))));
            this.panel_moved.Controls.Add(this.label2);
            this.panel_moved.Controls.Add(this.button_reduce);
            this.panel_moved.Controls.Add(this.button_exit);
            this.panel_moved.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_moved.Location = new System.Drawing.Point(0, 0);
            this.panel_moved.Name = "panel_moved";
            this.panel_moved.Size = new System.Drawing.Size(1146, 27);
            this.panel_moved.TabIndex = 0;
            this.panel_moved.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseDown);
            this.panel_moved.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseMove);
            this.panel_moved.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_moved_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(8, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 19);
            this.label2.TabIndex = 79;
            this.label2.Text = "Communauté Spirituelle";
            // 
            // button_reduce
            // 
            this.button_reduce.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            this.button_reduce.FlatAppearance.BorderSize = 0;
            this.button_reduce.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_reduce.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_reduce.ForeColor = System.Drawing.Color.White;
            this.button_reduce.Image = ((System.Drawing.Image)(resources.GetObject("button_reduce.Image")));
            this.button_reduce.Location = new System.Drawing.Point(1084, -1);
            this.button_reduce.Name = "button_reduce";
            this.button_reduce.Size = new System.Drawing.Size(31, 28);
            this.button_reduce.TabIndex = 16;
            this.button_reduce.UseVisualStyleBackColor = false;
            this.button_reduce.Click += new System.EventHandler(this.button_reduce_Click);
            // 
            // button_exit
            // 
            this.button_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            this.button_exit.FlatAppearance.BorderSize = 0;
            this.button_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_exit.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_exit.ForeColor = System.Drawing.Color.White;
            this.button_exit.Image = ((System.Drawing.Image)(resources.GetObject("button_exit.Image")));
            this.button_exit.Location = new System.Drawing.Point(1114, -2);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(31, 29);
            this.button_exit.TabIndex = 14;
            this.button_exit.UseVisualStyleBackColor = false;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // timerSideBar
            // 
            this.timerSideBar.Interval = 10;
            this.timerSideBar.Tick += new System.EventHandler(this.timerSideBar_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1146, 684);
            this.Controls.Add(this.panel_moved);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.SideBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Projects";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.SideBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Menu_moved)).EndInit();
            this.panel_moved.ResumeLayout(false);
            this.panel_moved.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel SideBar;
        private System.Windows.Forms.Button menu_list_bapteme;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button menu_list_user;
        private System.Windows.Forms.FlowLayoutPanel SidePanel;
        private System.Windows.Forms.Button menu_deconnexion;
        private System.Windows.Forms.Panel panel_moved;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Button button_reduce;
        private System.Windows.Forms.Button menu_welcome;
        private System.Windows.Forms.Button menu_list_confirmation;
        private System.Windows.Forms.Button menu_list_communion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox Menu_moved;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerSideBar;
        private System.Windows.Forms.Button menu_personne;
    }
}