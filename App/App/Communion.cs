﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Loginform;

namespace App
{
    public partial class Communion : Form
    {
        private Timer timer;
        private bool keyWasPressed = false;

        public Communion()
        {
            InitializeComponent();

            timer = new Timer();
            timer.Interval = 800;
            timer.Tick += Timer_Tick;

            textRecherche.KeyUp += TextRecherche_Key_up;
            textAnnee.KeyUp += TextRecherche_Key_up;
        }

        public void Open_Communion()
        {
            AjoutCommunion ajoutCommunion = new AjoutCommunion(this);
            ajoutCommunion.Show();

            ModifierCommunion modifierCommunion = new ModifierCommunion(this);
            modifierCommunion.Show();
        }

        public static class EditCommunionId
        {
            public static int Id { get; set; }
        }

        private void Communion_Load(object sender, EventArgs e)
        {
            searchMonth();

            show_data_communion();

            showhide_button_action();

            textRecherche.Text = "Recherche par nom ou prénom de la personne";
            textRecherche.ForeColor = Color.LightGray;
            textRecherche.BackColor = Color.White;

            textAnnee.Text = "Année";
            textAnnee.ForeColor = Color.LightGray;
            textAnnee.BackColor = Color.White;

            select_month.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void textRecherche_Leave(object sender, EventArgs e)
        {
            if (textRecherche.Text == "")
            {
                textRecherche.Text = "Recherche par nom ou prénom de la personne";
                textRecherche.ForeColor = Color.LightGray;
                textRecherche.BackColor = Color.White;
            }
        }

        private void textRecherche_Enter(object sender, EventArgs e)
        {
            if (textRecherche.Text == "Recherche par nom ou prénom de la personne")
            {
                textRecherche.Text = "";
                textRecherche.ForeColor = Color.Black;
                textRecherche.BackColor = Color.WhiteSmoke;
            }
        }

        public void show_data_communion() 
        {
            table_communion.Rows.Clear();
            table_communion.ReadOnly = true;
            

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT c.id_communion, p.last_name, p.first_name, c.date_communion FROM communion AS c " +
                    "INNER JOIN personne AS p ON p.id_personne = c.id_personne";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        while (readerList.Read())
                        {
                            object[] rowData = new object[readerList.FieldCount];
                            readerList.GetValues(rowData);

                            for (int i = 0; i < rowData.Length; i++)
                            {
                                if (rowData[i] is DateTime)
                                {
                                    rowData[i] = ((DateTime)rowData[i]).ToString("dd/MM/yyyy");
                                }
                            }

                            table_communion.Rows.Add(rowData);
                            table_communion.ClearSelection();
                        }
                    }
                }

                int nombreResultats = table_communion.Rows.Count;
                labelResultat.Text = "Résultat(s) : " + nombreResultats + " enregistré(s)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void showhide_button_action()
        {
            string Profil = Session.Profil;

            if (Profil == "Administrateur")
            {
                button_ajouter.Visible = true;
                button_modifier.Visible = true;
                button_supprimer.Visible = true;
            }
            else
            {
                button_ajouter.Visible = false;
                button_modifier.Visible = false;
                button_supprimer.Visible = false;
                label_alert_user.Visible = true;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            keyWasPressed = false;

            string recherche = textRecherche.Text;
            string annee = textAnnee.Text;
            int selectedMonthId = (int)select_month.SelectedValue;

            if (!string.IsNullOrEmpty(recherche) || !string.IsNullOrEmpty(annee) || recherche != "Recherche par nom ou prénom de la personne" || annee != "Année" || selectedMonthId != 0)
            {
                rechercheCommunion(recherche, annee, selectedMonthId);
            }
            else
            {
                show_data_communion();
            }
        }

        private void TextRecherche_Key_up(object sender, KeyEventArgs e)
        {
            keyWasPressed = true;
            timer.Stop();
            timer.Start();
        }

        private void rechercheCommunion(string recherche, string annee, int month)
        {
            table_communion.Rows.Clear();
            table_communion.ReadOnly = true;

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT c.id_communion, p.last_name, p.first_name, c.date_communion FROM communion AS c " +
                    "INNER JOIN personne AS p ON p.id_personne = c.id_personne WHERE 1 = 1";

                if (!string.IsNullOrEmpty(recherche) && recherche != "Recherche par nom ou prénom de la personne")
                {
                    query += " AND (p.last_name LIKE @lastnameStartsWith OR p.first_name LIKE @firstnameStartsWith)";
                }

                if (!string.IsNullOrEmpty(annee) && annee != "Année")
                {
                    query += " AND strftime('%Y', c.date_communion) = @annee";
                }

                if (month != 0)
                {
                    query += " AND strftime('%m', c.date_communion) = @month";
                }

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    if (!string.IsNullOrEmpty(recherche) && recherche != "Recherche par nom ou prénom de la personne")
                    {
                        command.Parameters.AddWithValue("@lastnameStartsWith", "%" + recherche + "%");
                        command.Parameters.AddWithValue("@firstnameStartsWith", "%" + recherche + "%");
                    }

                    if (!string.IsNullOrEmpty(annee) && annee != "Année")
                    {
                        command.Parameters.AddWithValue("@annee", annee);
                    }

                    if (month != 0)
                    {
                        command.Parameters.AddWithValue("@month", month.ToString().PadLeft(2, '0'));
                    }

                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        while (readerList.Read())
                        {
                            object[] rowData = new object[readerList.FieldCount];
                            readerList.GetValues(rowData);

                            for (int i = 0; i < rowData.Length; i++)
                            {
                                if (rowData[i] is DateTime)
                                {
                                    rowData[i] = ((DateTime)rowData[i]).ToString("dd/MM/yyyy");
                                }
                            }

                            table_communion.Rows.Add(rowData);
                            table_communion.ClearSelection();
                        }
                    }
                }

                int nombreResultats = table_communion.Rows.Count;
                labelResultat.Text = "Résultat(s) : " + nombreResultats + " enregistré(s)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_ajouter_Click(object sender, EventArgs e)
        {
            AjoutCommunion ajoutCommunion = new AjoutCommunion(this);
            ajoutCommunion.ShowDialog();
        }

        private string GetSelectedRowID()
        {
            if (table_communion.SelectedRows.Count > 0)
            {
                string selectedID = table_communion.SelectedRows[0].Cells[0].Value.ToString();
                return selectedID;
            }
            else
            {
                return null;
            }
        }

        private void table_communion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                DataGridViewRow selectedRow = table_communion.Rows[e.RowIndex];
                selectedRow.Selected = true;
            }
        }

        private void button_supprimer_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DialogResult result = MessageBox.Show("Êtes-vous sûr de vouloir supprimer cette personne de la communion ?", "Confirmation de suppression", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    try
                    {
                        string deleteQuery = "DELETE FROM communion WHERE id_communion = @id";

                        using (SQLiteCommand deleteCommand = new SQLiteCommand(deleteQuery, connection))
                        {
                            deleteCommand.Parameters.AddWithValue("@id", selectedID);

                            int rowsAffected = deleteCommand.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                MessageBox.Show("La suppression de la personne dans le menu communion a été effectuée avec succès !", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                show_data_communion();
                            }
                            else
                            {
                                MessageBox.Show("La suppression de la personne a échoué.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erreur lors de la suppression de la personne: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void button_modifier_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                EditCommunionId.Id = Convert.ToInt32(selectedID);

                ModifierCommunion modifierCommunion = new ModifierCommunion(this);
                modifierCommunion.ShowDialog();
            }
        }

        private void textAnnee_Enter(object sender, EventArgs e)
        {
            if (textAnnee.Text == "Année")
            {
                textAnnee.Text = "";
                textAnnee.ForeColor = Color.Black;
                textAnnee.BackColor = Color.WhiteSmoke;
            }
        }

        private void textAnnee_Leave(object sender, EventArgs e)
        {
            if (textAnnee.Text == "")
            {
                textAnnee.Text = "Année";
                textAnnee.ForeColor = Color.LightGray;
                textAnnee.BackColor = Color.White;
            }
        }

        private void searchMonth()
        {
            Dictionary<int, string> mois = new Dictionary<int, string>
            {
                { 0, "Tous" },
                { 1, "Janvier" },
                { 2, "Février" },
                { 3, "Mars" },
                { 4, "Avril" },
                { 5, "Mai" },
                { 6, "Juin" },
                { 7, "Juillet" },
                { 8, "Août" },
                { 9, "Septembre" },
                { 10, "Octobre" },
                { 11, "Novembre" },
                { 12, "Décembre" }
            };

            select_month.DisplayMember = "Value";
            select_month.ValueMember = "Key";
            select_month.DataSource = new BindingSource(mois, null);
        }

        private void textAnnee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void select_month_SelectedIndexChanged(object sender, EventArgs e)
        {
            string recherche = textRecherche.Text;
            string annee = textAnnee.Text;
            int selectedMonthId = (int)select_month.SelectedValue;

            if (!string.IsNullOrEmpty(recherche) || !string.IsNullOrEmpty(annee) || recherche != "Recherche par nom ou prénom de la personne" || annee != "Année" || selectedMonthId != 0)
            {
                rechercheCommunion(recherche, annee, selectedMonthId);
            }
            else
            {
                show_data_communion();
            }
        }
    }
}
