﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    internal class SQLiteDatabaseConneciton
    {
        private static SQLiteConnection _connection;
        private static readonly object _padLock = new object();
        private static readonly string connectionString = "Data Source=church_source.sqlite;Version=3;";

        public static SQLiteConnection GetConnection()
        {
            lock (_padLock)
            {
                if (_connection == null)
                {
                    _connection = new SQLiteConnection(connectionString);
                    _connection.Open();
                }

                return _connection;
            }
        }
    }
}
