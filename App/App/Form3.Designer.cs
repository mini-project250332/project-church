﻿namespace App
{
    partial class forgetPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(forgetPassword));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textParaph = new System.Windows.Forms.TextBox();
            this.checkUsername = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button_retour = new System.Windows.Forms.Button();
            this.button_verifier = new System.Windows.Forms.Button();
            this.button_exit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bodoni MT", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(113, 235);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 32);
            this.label1.TabIndex = 17;
            this.label1.Text = "Mot de passe oublié";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(97, 38);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(289, 176);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // textParaph
            // 
            this.textParaph.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textParaph.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textParaph.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textParaph.Location = new System.Drawing.Point(56, 290);
            this.textParaph.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textParaph.Multiline = true;
            this.textParaph.Name = "textParaph";
            this.textParaph.Size = new System.Drawing.Size(373, 80);
            this.textParaph.TabIndex = 36;
            // 
            // checkUsername
            // 
            this.checkUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkUsername.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkUsername.ForeColor = System.Drawing.SystemColors.Desktop;
            this.checkUsername.Location = new System.Drawing.Point(112, 386);
            this.checkUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkUsername.Multiline = true;
            this.checkUsername.Name = "checkUsername";
            this.checkUsername.ShortcutsEnabled = false;
            this.checkUsername.Size = new System.Drawing.Size(317, 30);
            this.checkUsername.TabIndex = 39;
            this.checkUsername.Enter += new System.EventHandler(this.checkUsername_Enter);
            this.checkUsername.Leave += new System.EventHandler(this.checkUsername_Leave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel2.Location = new System.Drawing.Point(57, 418);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(372, 1);
            this.panel2.TabIndex = 37;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(56, 375);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 37);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 38;
            this.pictureBox3.TabStop = false;
            // 
            // button_retour
            // 
            this.button_retour.BackColor = System.Drawing.Color.Firebrick;
            this.button_retour.FlatAppearance.BorderSize = 0;
            this.button_retour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_retour.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_retour.ForeColor = System.Drawing.Color.White;
            this.button_retour.Location = new System.Drawing.Point(56, 473);
            this.button_retour.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_retour.Name = "button_retour";
            this.button_retour.Size = new System.Drawing.Size(156, 50);
            this.button_retour.TabIndex = 41;
            this.button_retour.Text = "Retour";
            this.button_retour.UseVisualStyleBackColor = false;
            this.button_retour.Click += new System.EventHandler(this.button_retour_Click);
            // 
            // button_verifier
            // 
            this.button_verifier.BackColor = System.Drawing.SystemColors.Highlight;
            this.button_verifier.FlatAppearance.BorderSize = 0;
            this.button_verifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_verifier.Font = new System.Drawing.Font("Bodoni MT Condensed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_verifier.ForeColor = System.Drawing.Color.White;
            this.button_verifier.Location = new System.Drawing.Point(272, 473);
            this.button_verifier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_verifier.Name = "button_verifier";
            this.button_verifier.Size = new System.Drawing.Size(156, 50);
            this.button_verifier.TabIndex = 40;
            this.button_verifier.Text = "Vérifier";
            this.button_verifier.UseVisualStyleBackColor = false;
            this.button_verifier.Click += new System.EventHandler(this.button_verifier_Click);
            // 
            // button_exit
            // 
            this.button_exit.BackColor = System.Drawing.Color.Firebrick;
            this.button_exit.FlatAppearance.BorderSize = 0;
            this.button_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_exit.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_exit.ForeColor = System.Drawing.Color.White;
            this.button_exit.Image = ((System.Drawing.Image)(resources.GetObject("button_exit.Image")));
            this.button_exit.Location = new System.Drawing.Point(445, -1);
            this.button_exit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(41, 32);
            this.button_exit.TabIndex = 42;
            this.button_exit.UseVisualStyleBackColor = false;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // forgetPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(487, 553);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.button_retour);
            this.Controls.Add(this.button_verifier);
            this.Controls.Add(this.checkUsername);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textParaph);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "forgetPassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mot de passe oublié";
            this.Load += new System.EventHandler(this.forgetPassword_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.forgetPassword_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.forgetPassword_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.forgetPassword_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textParaph;
        private System.Windows.Forms.TextBox checkUsername;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button_retour;
        private System.Windows.Forms.Button button_verifier;
        private System.Windows.Forms.Button button_exit;
    }
}