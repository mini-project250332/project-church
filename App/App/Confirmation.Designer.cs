﻿namespace App
{
    partial class Confirmation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Confirmation));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_btn_action = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textRecherche = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelResultat = new System.Windows.Forms.Label();
            this.label_alert_user = new System.Windows.Forms.Label();
            this.button_supprimer = new System.Windows.Forms.Button();
            this.button_modifier = new System.Windows.Forms.Button();
            this.button_ajouter = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.table_confirmation = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pane_tableau = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.textAnnee = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.select_month = new System.Windows.Forms.ComboBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel_btn_action.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_confirmation)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(922, 62);
            this.panel1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(21, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Confirmation";
            // 
            // panel_btn_action
            // 
            this.panel_btn_action.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_btn_action.AutoSize = true;
            this.panel_btn_action.BackColor = System.Drawing.Color.White;
            this.panel_btn_action.Controls.Add(this.pictureBox2);
            this.panel_btn_action.Controls.Add(this.textAnnee);
            this.panel_btn_action.Controls.Add(this.panel4);
            this.panel_btn_action.Controls.Add(this.label4);
            this.panel_btn_action.Controls.Add(this.pictureBox1);
            this.panel_btn_action.Controls.Add(this.select_month);
            this.panel_btn_action.Controls.Add(this.panel6);
            this.panel_btn_action.Controls.Add(this.label2);
            this.panel_btn_action.Controls.Add(this.pictureBox6);
            this.panel_btn_action.Controls.Add(this.label3);
            this.panel_btn_action.Controls.Add(this.textRecherche);
            this.panel_btn_action.Controls.Add(this.panel5);
            this.panel_btn_action.Location = new System.Drawing.Point(0, 69);
            this.panel_btn_action.Name = "panel_btn_action";
            this.panel_btn_action.Size = new System.Drawing.Size(922, 56);
            this.panel_btn_action.TabIndex = 15;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(123, 13);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(35, 27);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 100;
            this.pictureBox6.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 23);
            this.label3.TabIndex = 99;
            this.label3.Text = "Recherche :";
            // 
            // textRecherche
            // 
            this.textRecherche.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textRecherche.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textRecherche.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRecherche.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textRecherche.Location = new System.Drawing.Point(160, 17);
            this.textRecherche.Name = "textRecherche";
            this.textRecherche.Size = new System.Drawing.Size(285, 19);
            this.textRecherche.TabIndex = 98;
            this.textRecherche.Enter += new System.EventHandler(this.textRecherche_Enter);
            this.textRecherche.Leave += new System.EventHandler(this.textRecherche_Leave);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel5.Location = new System.Drawing.Point(122, 43);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(325, 1);
            this.panel5.TabIndex = 97;
            // 
            // labelResultat
            // 
            this.labelResultat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelResultat.AutoSize = true;
            this.labelResultat.BackColor = System.Drawing.Color.White;
            this.labelResultat.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultat.ForeColor = System.Drawing.Color.Black;
            this.labelResultat.Location = new System.Drawing.Point(23, 24);
            this.labelResultat.Name = "labelResultat";
            this.labelResultat.Size = new System.Drawing.Size(0, 23);
            this.labelResultat.TabIndex = 14;
            // 
            // label_alert_user
            // 
            this.label_alert_user.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_alert_user.AutoSize = true;
            this.label_alert_user.BackColor = System.Drawing.Color.White;
            this.label_alert_user.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_alert_user.ForeColor = System.Drawing.Color.Black;
            this.label_alert_user.Location = new System.Drawing.Point(322, 24);
            this.label_alert_user.Name = "label_alert_user";
            this.label_alert_user.Size = new System.Drawing.Size(588, 23);
            this.label_alert_user.TabIndex = 13;
            this.label_alert_user.Text = "Seuls les profils \"Administrateur\" peuvent faire des actions sur le logiciel";
            this.label_alert_user.Visible = false;
            // 
            // button_supprimer
            // 
            this.button_supprimer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_supprimer.BackColor = System.Drawing.Color.IndianRed;
            this.button_supprimer.FlatAppearance.BorderSize = 0;
            this.button_supprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_supprimer.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_supprimer.ForeColor = System.Drawing.Color.White;
            this.button_supprimer.Image = ((System.Drawing.Image)(resources.GetObject("button_supprimer.Image")));
            this.button_supprimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_supprimer.Location = new System.Drawing.Point(767, 12);
            this.button_supprimer.Name = "button_supprimer";
            this.button_supprimer.Size = new System.Drawing.Size(143, 41);
            this.button_supprimer.TabIndex = 11;
            this.button_supprimer.Text = "      Supprimer";
            this.button_supprimer.UseVisualStyleBackColor = false;
            this.button_supprimer.Click += new System.EventHandler(this.button_supprimer_Click);
            // 
            // button_modifier
            // 
            this.button_modifier.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button_modifier.BackColor = System.Drawing.Color.Gray;
            this.button_modifier.FlatAppearance.BorderSize = 0;
            this.button_modifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_modifier.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_modifier.ForeColor = System.Drawing.Color.White;
            this.button_modifier.Image = ((System.Drawing.Image)(resources.GetObject("button_modifier.Image")));
            this.button_modifier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_modifier.Location = new System.Drawing.Point(597, 13);
            this.button_modifier.Name = "button_modifier";
            this.button_modifier.Size = new System.Drawing.Size(143, 41);
            this.button_modifier.TabIndex = 10;
            this.button_modifier.Text = "      Modifier";
            this.button_modifier.UseVisualStyleBackColor = false;
            this.button_modifier.Click += new System.EventHandler(this.button_modifier_Click);
            // 
            // button_ajouter
            // 
            this.button_ajouter.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button_ajouter.BackColor = System.Drawing.SystemColors.Highlight;
            this.button_ajouter.FlatAppearance.BorderSize = 0;
            this.button_ajouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ajouter.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ajouter.ForeColor = System.Drawing.Color.White;
            this.button_ajouter.Image = ((System.Drawing.Image)(resources.GetObject("button_ajouter.Image")));
            this.button_ajouter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_ajouter.Location = new System.Drawing.Point(434, 13);
            this.button_ajouter.Name = "button_ajouter";
            this.button_ajouter.Size = new System.Drawing.Size(135, 41);
            this.button_ajouter.TabIndex = 9;
            this.button_ajouter.Text = "        Ajouter";
            this.button_ajouter.UseVisualStyleBackColor = false;
            this.button_ajouter.Click += new System.EventHandler(this.button_ajouter_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.table_confirmation);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 131);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(922, 453);
            this.panel3.TabIndex = 18;
            // 
            // table_confirmation
            // 
            this.table_confirmation.AllowUserToAddRows = false;
            this.table_confirmation.AllowUserToDeleteRows = false;
            this.table_confirmation.AllowUserToResizeColumns = false;
            this.table_confirmation.AllowUserToResizeRows = false;
            this.table_confirmation.BackgroundColor = System.Drawing.Color.White;
            this.table_confirmation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.table_confirmation.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.table_confirmation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.table_confirmation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_confirmation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column5});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(57)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.table_confirmation.DefaultCellStyle = dataGridViewCellStyle10;
            this.table_confirmation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.table_confirmation.EnableHeadersVisualStyles = false;
            this.table_confirmation.GridColor = System.Drawing.Color.Silver;
            this.table_confirmation.Location = new System.Drawing.Point(0, 0);
            this.table_confirmation.Name = "table_confirmation";
            this.table_confirmation.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.table_confirmation.RowHeadersVisible = false;
            this.table_confirmation.RowHeadersWidth = 51;
            this.table_confirmation.Size = new System.Drawing.Size(922, 453);
            this.table_confirmation.TabIndex = 1;
            this.table_confirmation.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_confirmation_CellClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.FillWeight = 10F;
            this.Column1.HeaderText = "ID";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 51;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.FillWeight = 30F;
            this.Column2.HeaderText = "Nom";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.FillWeight = 60F;
            this.Column3.HeaderText = "Prénom";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column5.FillWeight = 50F;
            this.Column5.HeaderText = "Date de la Confirmation";
            this.Column5.Name = "Column5";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.labelResultat);
            this.panel2.Controls.Add(this.label_alert_user);
            this.panel2.Controls.Add(this.button_supprimer);
            this.panel2.Controls.Add(this.button_modifier);
            this.panel2.Controls.Add(this.button_ajouter);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 584);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(922, 61);
            this.panel2.TabIndex = 17;
            // 
            // pane_tableau
            // 
            this.pane_tableau.AutoSize = true;
            this.pane_tableau.BackColor = System.Drawing.Color.White;
            this.pane_tableau.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pane_tableau.Location = new System.Drawing.Point(0, 645);
            this.pane_tableau.Name = "pane_tableau";
            this.pane_tableau.Size = new System.Drawing.Size(922, 0);
            this.pane_tableau.TabIndex = 16;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(749, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(35, 27);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 127;
            this.pictureBox2.TabStop = false;
            // 
            // textAnnee
            // 
            this.textAnnee.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textAnnee.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textAnnee.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAnnee.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textAnnee.Location = new System.Drawing.Point(789, 16);
            this.textAnnee.Name = "textAnnee";
            this.textAnnee.Size = new System.Drawing.Size(119, 19);
            this.textAnnee.TabIndex = 126;
            this.textAnnee.Enter += new System.EventHandler(this.textAnnee_Enter);
            this.textAnnee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textAnnee_KeyPress);
            this.textAnnee.Leave += new System.EventHandler(this.textAnnee_Leave);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel4.Location = new System.Drawing.Point(750, 43);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(159, 1);
            this.panel4.TabIndex = 125;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(681, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 23);
            this.label4.TabIndex = 124;
            this.label4.Text = "Année :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(514, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 123;
            this.pictureBox1.TabStop = false;
            // 
            // select_month
            // 
            this.select_month.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.select_month.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.select_month.ForeColor = System.Drawing.SystemColors.ControlText;
            this.select_month.Location = new System.Drawing.Point(554, 13);
            this.select_month.Margin = new System.Windows.Forms.Padding(2);
            this.select_month.Name = "select_month";
            this.select_month.Size = new System.Drawing.Size(109, 25);
            this.select_month.TabIndex = 122;
            this.select_month.SelectedIndexChanged += new System.EventHandler(this.select_month_SelectedIndexChanged);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel6.Location = new System.Drawing.Point(514, 43);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(149, 1);
            this.panel6.TabIndex = 121;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(455, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 23);
            this.label2.TabIndex = 120;
            this.label2.Text = "Mois :";
            // 
            // Confirmation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(922, 645);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel_btn_action);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pane_tableau);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Confirmation";
            this.Text = "Confirmation";
            this.Load += new System.EventHandler(this.Confirmation_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel_btn_action.ResumeLayout(false);
            this.panel_btn_action.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table_confirmation)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel_btn_action;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textRecherche;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label labelResultat;
        private System.Windows.Forms.Label label_alert_user;
        private System.Windows.Forms.Button button_supprimer;
        private System.Windows.Forms.Button button_modifier;
        private System.Windows.Forms.Button button_ajouter;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView table_confirmation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pane_tableau;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox textAnnee;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox select_month;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2;
    }
}