﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using static App.forgetPassword;

namespace App
{
    public partial class newPassword : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public newPassword()
        {
            InitializeComponent();
        }

        private void newPassword_Load(object sender, EventArgs e)
        {
            textPasswordCreate.Text = "Entrez votre mot de passe";
            textPasswordConfirm.Text = "Confirmer votre mot de passe";
            textPasswordCreate.ForeColor = Color.LightGray;
            textPasswordConfirm.ForeColor = Color.LightGray;
        }
        private void textPasswordCreate_Enter(object sender, EventArgs e)
        {
            if (textPasswordCreate.Text == "Entrez votre mot de passe")
            {
                textPasswordCreate.Text = "";
                textPasswordCreate.ForeColor = Color.Black;
                textPasswordCreate.PasswordChar = '*';
            }
        }

        private void textPasswordCreate_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPasswordCreate.Text))
            {
                textPasswordCreate.Text = "Entrez votre mot de passe";
                textPasswordCreate.ForeColor = Color.LightGray;
                textPasswordCreate.PasswordChar = '\0';
            }
        }

        private void textPasswordConfirm_Enter(object sender, EventArgs e)
        {
            if (textPasswordConfirm.Text == "Confirmer votre mot de passe")
            {
                textPasswordConfirm.Text = "";
                textPasswordConfirm.ForeColor = Color.Black;
                textPasswordConfirm.PasswordChar = '*';
            }
        }

        private void textPasswordConfirm_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPasswordConfirm.Text))
            {
                textPasswordConfirm.Text = "Confirmer votre mot de passe";
                textPasswordConfirm.ForeColor = Color.LightGray;
                textPasswordConfirm.PasswordChar = '\0';
            }
        }

        private void button_verifier_Click(object sender, EventArgs e)
        {
            string password = textPasswordCreate.Text;
            string confirmPassword = textPasswordConfirm.Text;
            int UserId = PassId.UserId;

            if (string.IsNullOrEmpty(password) ||
                string.IsNullOrEmpty(confirmPassword) ||
                password == "Entrez votre mot de passe" ||
                confirmPassword == "Confirmer votre mot de passe")
            {
                MessageBox.Show("Veuillez remplir tous les champs.","Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // if password and confirm password isn't the same
            if (password != confirmPassword)
            {
                MessageBox.Show("Les mots de passe ne correspondent pas.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Insert the new password into the database for the specified userId
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "UPDATE users SET password = @password WHERE id = @userId";
                using (SQLiteCommand commandUpdate = new SQLiteCommand(query, connection))
                {
                    commandUpdate.Parameters.AddWithValue("@password", password);
                    commandUpdate.Parameters.AddWithValue("@userId", UserId);

                    int rowsAffected = commandUpdate.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Mot de passe mis à jour avec succès.", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // Hide the login form4
                        this.Hide();

                        // Create and show Form1
                        Loginform Loginform = new Loginform();
                        Loginform.ShowDialog();

                        // Close the application when Form4 is closed (optional)
                        Application.Exit();
                    }
                    else
                    {
                        MessageBox.Show("Erreur lors de la mise à jour du mot de passe.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newPassword_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void newPassword_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void newPassword_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();

            Loginform Loginform = new Loginform();
            Loginform.ShowDialog();

            Application.Exit();
        }
    }
}
