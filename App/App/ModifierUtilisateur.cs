﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Utilisateurs;

namespace App
{
    public partial class ModifierUtilisateur : Form
    {
        private Utilisateurs _formUserReference;

        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public ModifierUtilisateur(Utilisateurs formUserReference)
        {
            InitializeComponent();

            _formUserReference = formUserReference;
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void textFirstName_Enter(object sender, EventArgs e)
        {
            if (textFirstName.Text == "Entrez votre nom")
            {
                textFirstName.Text = "";
                textFirstName.ForeColor = Color.Black;
            }
        }

        private void textFirstName_Leave(object sender, EventArgs e)
        {

            if (textFirstName.Text == "")
            {
                textFirstName.Text = "Entrez votre nom";
                textFirstName.ForeColor = Color.LightGray;
            }
        }

        private void textLastName_Leave(object sender, EventArgs e)
        {
            if (textLastName.Text == "")
            {
                textLastName.Text = "Entrez votre prénom";
                textLastName.ForeColor = Color.LightGray;
            }
        }

        private void textLastName_Enter(object sender, EventArgs e)
        {
            if (textLastName.Text == "Entrez votre prénom")
            {
                textLastName.Text = "";
                textLastName.ForeColor = Color.Black;
            }
        }

        private void textLoginUser_Leave(object sender, EventArgs e)
        {
            if (textLoginUser.Text == "")
            {
                textLoginUser.Text = "Entrez votre identifiant";
                textLoginUser.ForeColor = Color.LightGray;
            }
        }

        private void textLoginUser_Enter(object sender, EventArgs e)
        {
            if (textLoginUser.Text == "Entrez votre identifiant")
            {
                textLoginUser.Text = "";
                textLoginUser.ForeColor = Color.Black;
            }
        }

        private void textPasswordCreate_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textPasswordCreate.Text))
            {
                textPasswordCreate.Text = "Entrez votre mot de passe";
                textPasswordCreate.ForeColor = Color.LightGray;
                textPasswordCreate.PasswordChar = '\0';
            }
        }

        private void textPasswordCreate_Enter(object sender, EventArgs e)
        {
            if (textPasswordCreate.Text == "Entrez votre mot de passe")
            {
                textPasswordCreate.Text = "";
                textPasswordCreate.ForeColor = Color.Black;
                textPasswordCreate.PasswordChar = '*';
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void ModifierUtilisateur_Load(object sender, EventArgs e)
        {
            int userid = EditUserid.UserId;

            show_user_by_id(userid);
        }

        private void show_user_by_id(int id)
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT * FROM users WHERE id = @id";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    
                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int userid = Convert.ToInt32(reader["id"]);
                            string first_name = reader["first_name"].ToString();
                            string last_name = reader["last_name"].ToString();
                            string username = reader["username"].ToString();
                            string profil = reader["profil"].ToString();
                            string password = reader["password"].ToString();

                            textFirstName.Text = first_name;
                            textLastName.Text = last_name;
                            textLoginUser.Text = username;
                            textPasswordCreate.Text = password;
                            profil_select.SelectedItem = profil;

                            textFirstName.ForeColor = Color.Black;
                            textLastName.ForeColor = Color.Black;
                            textLoginUser.ForeColor = Color.Black;
                            textPasswordCreate.ForeColor = Color.Black;

                            textPasswordCreate.PasswordChar = '*';
                            profil_select.DropDownStyle = ComboBoxStyle.DropDownList;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            int userid = EditUserid.UserId;
            string username = textLoginUser.Text;
            string password = textPasswordCreate.Text;
            string firstName = textFirstName.Text;
            string lastName = textLastName.Text;
            string profil = profil_select.SelectedItem?.ToString();

            // Validate inputs
            if (string.IsNullOrEmpty(username) ||
                string.IsNullOrEmpty(password) ||
                string.IsNullOrEmpty(firstName) ||
                string.IsNullOrEmpty(lastName) ||
                username == "Entrez votre nom" ||
                password == "Entrez votre mot de passe" ||
                firstName == "Entrez votre nom" ||
                lastName == "Entrez votre prénom")
            {
                MessageBox.Show("Veuillez remplir tous les champs.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "UPDATE users SET last_name = @lastname, first_name = @firstname, profil = @profil,"+
                    "username = @username, password = @password WHERE id = @userId";

                using (SQLiteCommand commandUpdate = new SQLiteCommand(query, connection))
                {
                    commandUpdate.Parameters.AddWithValue("@password", password);
                    commandUpdate.Parameters.AddWithValue("@userId", userid);
                    commandUpdate.Parameters.AddWithValue("@firstname", firstName);
                    commandUpdate.Parameters.AddWithValue("@lastname", lastName);
                    commandUpdate.Parameters.AddWithValue("@profil", profil);
                    commandUpdate.Parameters.AddWithValue("@username", username);

                    int rowsAffected = commandUpdate.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Modification de l'utilisateur avec succès", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Hide();

                        _formUserReference.show_data_utilisateur();
                    }
                    else
                    {
                        MessageBox.Show("Erreur lors de la modification.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
