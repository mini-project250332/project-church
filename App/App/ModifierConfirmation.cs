﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Confirmation;

namespace App
{
    public partial class ModifierConfirmation : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        private Confirmation _formConfirmationReference;

        public ModifierConfirmation(Confirmation formConfirmationReference)
        {
            InitializeComponent();

            _formConfirmationReference = formConfirmationReference;
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void textPriestName_Enter(object sender, EventArgs e)
        {
            if (textPriestName.Text == "Entrez le nom du prêtre")
            {
                textPriestName.Text = "";
                textPriestName.ForeColor = Color.Black;
            }
        }

        private void textPriestName_Leave(object sender, EventArgs e)
        {
            if (textPriestName.Text == "")
            {
                textPriestName.Text = "Entrez le nom du prêtre";
                textPriestName.ForeColor = Color.LightGray;
            }
        }

        private void textGodFather_Enter(object sender, EventArgs e)
        {
            if (textGodFather.Text == "Entrez le nom du parrain ou marraine")
            {
                textGodFather.Text = "";
                textGodFather.ForeColor = Color.Black;
            }
        }

        private void textGodFather_Leave(object sender, EventArgs e)
        {
            if (textGodFather.Text == "")
            {
                textGodFather.Text = "Entrez le nom du parrain ou marraine";
                textGodFather.ForeColor = Color.LightGray;
            }
        }

        private void ModifierConfirmation_Load(object sender, EventArgs e)
        {
            int id = EditConfirmationid.Id;

            show_Confirmation_by_id(id);
        }

        private void show_Confirmation_by_id(int id)
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT p.id_personne, c.id_confirmation, p.last_name, p.first_name, c.date_confirmation, c.priest, c.godfather FROM confirmation AS c " +
                    "INNER JOIN personne AS p ON p.id_personne = c.id_personne " +
                    "WHERE c.id_confirmation = @id";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", id);

                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        select_personne.Items.Clear();
                        select_personne.DropDownStyle = ComboBoxStyle.DropDownList;

                        List<Person> people = new List<Person>();

                        if (reader.Read())
                        {
                            int id_personne = Convert.ToInt32(reader["id_personne"]);
                            string first_name = reader["first_name"].ToString();
                            string last_name = reader["last_name"].ToString();
                            string priest = reader["priest"].ToString();
                            string godfather = reader["godfather"].ToString();
                            DateTime date_confirmation = Convert.ToDateTime(reader["date_confirmation"]);

                            string fullName = $"{last_name} {first_name}";
                            int idPersonne = Convert.ToInt32(id_personne);

                            people.Add(new Person(idPersonne, fullName));

                            select_personne.DataSource = people;
                            select_personne.DisplayMember = "FullName";
                            select_personne.ValueMember = "Id";

                            textPriestName.Text = priest;
                            textGodFather.Text = godfather;
                            select_personne.DropDownStyle = ComboBoxStyle.DropDownList;
                            dateConfirmation.Value = date_confirmation;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            int id = EditConfirmationid.Id;
            string priest = textPriestName.Text;
            string godfather = textGodFather.Text;
            DateTime date_confirmation = dateConfirmation.Value.Date;
            int id_personne;

            if (select_personne != null && select_personne.SelectedItem != null)
            {
                id_personne = ((Person)select_personne.SelectedItem).Id;
            }
            else
            {
                MessageBox.Show("La liste déroulante est vide.","Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(priest) || priest == "Entrez le nom du prêtre")
            {
                MessageBox.Show("Le champs nom du prêtre ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(godfather) || godfather == "Entrez le nom du parrain ou marraine")
            {
                MessageBox.Show("Le champs nom du parrain ou de la marraine ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (date_confirmation == null || dateConfirmation.Value == DateTime.MinValue)
            {
                MessageBox.Show("Veuillez sélectionner une date de la  confirmation.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "UPDATE confirmation SET priest = @priest, godfather = @godfather, date_confirmation = @date_confirmation," +
                    " id_personne = @id_personne WHERE id_confirmation = @id_confirmation";

                using (SQLiteCommand commandUpdate = new SQLiteCommand(query, connection))
                {

                    commandUpdate.Parameters.AddWithValue("@id_confirmation", id);
                    commandUpdate.Parameters.AddWithValue("@priest", priest);
                    commandUpdate.Parameters.AddWithValue("@godfather", godfather);
                    commandUpdate.Parameters.AddWithValue("@date_confirmation", date_confirmation);
                    commandUpdate.Parameters.AddWithValue("@id_personne", id_personne);

                    int rowsAffected = commandUpdate.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Modification de la Confirmation avec succès", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Hide();

                        _formConfirmationReference.show_data_confirmation();
                    }
                    else
                    {
                        MessageBox.Show("Erreur lors de la modification.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
