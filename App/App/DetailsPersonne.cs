﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using static App.Personnes;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace App
{
    public partial class DetailsPersonne : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        public DetailsPersonne()
        {
            InitializeComponent();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void DetailsPersonne_Load(object sender, EventArgs e)
        {
            int id = EditPersonneid.Id;

            ShowDetailsPersonne(id);
        }

        private void ShowDetailsPersonne(int id)
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT " +
                    "p.first_name AS First_Name, " +
                    "p.last_name AS Last_Name, " +
                    "p.father_name AS Father_Name, " +
                    "p.fathers_contact AS Fathers_Contact, " +
                    "p.mothers_name AS Mothers_Name, " +
                    "p.mothers_contact AS Mothers_Contact, " +
                    "p.sexe AS Sexe, " +
                    "p.date_birth AS Date_Birth, " +
                    "p.place_birth AS Place_Birth, " +
                    "p.address AS Address, " +
                    "b.date_bapteme AS Date_bapteme, " +
                    "b.priest AS PriestBapteme, " +
                    "b.godfather AS GodFatherBapteme, " +
                    "co.date_communion AS Date_communion, " +
                    "co.priest AS PriestCommunion, " +
                    "conf.date_confirmation AS date_Confirmation, " +
                    "conf.priest AS PriestConfirmation, " +
                    "conf.godfather AS godfatherConfirmation " +
                    "FROM personne AS p " +
                    "LEFT JOIN bapteme AS b ON b.id_personne = p.id_personne " +
                    "LEFT JOIN communion AS co ON co.id_personne = p.id_personne " +
                    "LEFT JOIN confirmation AS conf ON conf.id_personne = p.id_personne " +
                    "WHERE p.id_personne = @id";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", id);

                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            labelNom.Text = reader["Last_Name"].ToString();
                            labelPrenom.Text = reader["First_Name"].ToString();
                            labelSexe.Text = reader["Sexe"].ToString();
                            labelDateNaissance.Text = (reader["Date_Birth"] != DBNull.Value) ? Convert.ToDateTime(reader["Date_Birth"]).ToShortDateString() : "";
                            labelLieuNaissance.Text = reader["Place_Birth"].ToString();
                            labelNomPere.Text = reader["Father_Name"].ToString();
                            labelNumeroPere.Text = reader["Fathers_Contact"].ToString();
                            labelNomMere.Text = reader["Mothers_Name"].ToString();
                            labelNumeroMere.Text = reader["Mothers_Contact"].ToString();
                            labelAdresse.Text = reader["Address"].ToString();

                            labelNomPrereBapteme.Text = reader["PriestBapteme"].ToString();
                            labelNomParrainBapteme.Text = reader["GodFatherBapteme"].ToString();
                            labelDateBapteme.Text = (reader["Date_bapteme"] != DBNull.Value) ? Convert.ToDateTime(reader["Date_bapteme"]).ToShortDateString() : "";

                            labelNomPretreCommunion.Text = reader["PriestCommunion"].ToString();
                            labelDateCommunion.Text = (reader["Date_communion"] != DBNull.Value) ? Convert.ToDateTime(reader["Date_communion"]).ToShortDateString() : "";

                            labelNomPretreConfirmation.Text = reader["PriestConfirmation"].ToString();
                            labelNomParrainConfirmation.Text = reader["godfatherConfirmation"].ToString();
                            labelDateConfirmation.Text = (reader["date_Confirmation"] != DBNull.Value) ? Convert.ToDateTime(reader["date_Confirmation"]).ToShortDateString() : "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PDF_Click(object sender, EventArgs e)
        {
            try
            {
                string downloadsPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads";

                string outputPath = Path.Combine(downloadsPath, "example.pdf");

                using (FileStream fs = new FileStream(outputPath, FileMode.Create))
                {
                    using (Document document = new Document())
                    {
                        PdfWriter writer = PdfWriter.GetInstance(document, fs);

                        document.Open();

                        // Ajouter le texte des labels dans le PDF
                        AddFormattedParagraph(document, labelInfoPersonne.Text, true);
                        AddFormattedParagraph(document, $"Nom: {labelNom.Text}");
                        AddFormattedParagraph(document, $"Prénom: {labelPrenom.Text}");
                        AddFormattedParagraph(document, $"Sexe: {labelSexe.Text}");
                        AddFormattedParagraph(document, $"Date de Naissance: {labelDateNaissance.Text}");
                        AddFormattedParagraph(document, $"Lieu de Naissance: {labelLieuNaissance.Text}");
                        AddFormattedParagraph(document, $"Nom du Père: {labelNomPere.Text}");
                        AddFormattedParagraph(document, $"Numéro du Père: {labelNumeroPere.Text}");
                        AddFormattedParagraph(document, $"Nom de la Mère: {labelNomMere.Text}");
                        AddFormattedParagraph(document, $"Numéro de la Mère: {labelNumeroMere.Text}");
                        AddFormattedParagraph(document, $"Adresse: {labelAdresse.Text}");

                        AddFormattedParagraph(document, labelInfoBapteme.Text, true);
                        AddFormattedParagraph(document, $"Nom du Prêtre de Baptême: {labelNomPrereBapteme.Text}");
                        AddFormattedParagraph(document, $"Nom du Parrain de Baptême: {labelNomParrainBapteme.Text}");
                        AddFormattedParagraph(document, $"Date du Baptême: {labelDateBapteme.Text}");

                        AddFormattedParagraph(document, labelInfoCommunion.Text, true);
                        AddFormattedParagraph(document, $"Nom du Prêtre de Communion: {labelNomPretreCommunion.Text}");
                        AddFormattedParagraph(document, $"Date de la Communion: {labelDateCommunion.Text}");

                        AddFormattedParagraph(document, labelInfoConfirmation.Text, true);
                        AddFormattedParagraph(document, $"Nom du Prêtre de Confirmation: {labelNomPretreConfirmation.Text}");
                        AddFormattedParagraph(document, $"Nom du Parrain de Confirmation: {labelNomParrainConfirmation.Text}");
                        AddFormattedParagraph(document, $"Date de la Confirmation: {labelDateConfirmation.Text}");

                        document.Close();
                    }
                }

                MessageBox.Show("PDF généré avec succès dans le dossier Téléchargements.", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddFormattedParagraph(Document document, string text, bool underline = false)
        {
            Paragraph paragraph = new Paragraph();

            if (underline)
            {
                Chunk chunk = new Chunk(text, FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.UNDERLINE));
                paragraph.Add(chunk);

                Paragraph emptyLine = new Paragraph(" ");
                emptyLine.SpacingBefore = 4f;
                document.Add(emptyLine);
            }
            else
            {
                paragraph.Add(new Chunk(text, FontFactory.GetFont(FontFactory.HELVETICA, 12)));
            }

            document.Add(paragraph);
        }
    }
}
