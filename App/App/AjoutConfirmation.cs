﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    public partial class AjoutConfirmation : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        private Confirmation _formConfirmationReference;

        public AjoutConfirmation(Confirmation formConfirmationReference)
        {
            InitializeComponent();

            _formConfirmationReference = formConfirmationReference;
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void AjoutConfirmation_Load(object sender, EventArgs e)
        {
            textPriestName.Text = "Entrez le nom du prêtre";
            textPriestName.ForeColor = Color.LightGray;
            textGodFather.Text = "Entrez le nom du parrain ou marraine";
            textGodFather.ForeColor = Color.LightGray;

            getAllPersonneNoConfirmation();
        }

        private void textPriestName_Enter(object sender, EventArgs e)
        {
            if (textPriestName.Text == "Entrez le nom du prêtre")
            {
                textPriestName.Text = "";
                textPriestName.ForeColor = Color.Black;
            }
        }

        private void textPriestName_Leave(object sender, EventArgs e)
        {
            if (textPriestName.Text == "")
            {
                textPriestName.Text = "Entrez le nom du prêtre";
                textPriestName.ForeColor = Color.LightGray;
            }
        }

        private void textGodFather_Enter(object sender, EventArgs e)
        {
            if (textGodFather.Text == "Entrez le nom du parrain ou marraine")
            {
                textGodFather.Text = "";
                textGodFather.ForeColor = Color.Black;
            }
        }

        private void textGodFather_Leave(object sender, EventArgs e)
        {
            if (textGodFather.Text == "")
            {
                textGodFather.Text = "Entrez le nom du parrain ou marraine";
                textGodFather.ForeColor = Color.LightGray;
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void getAllPersonneNoConfirmation()
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT p.id_personne, p.last_name, p.first_name FROM personne AS p " +
               "LEFT JOIN confirmation AS c ON c.id_personne = p.id_personne " +
               "WHERE c.id_personne IS NULL ORDER BY p.last_name, p.first_name ASC";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        select_personne.Items.Clear();
                        select_personne.DropDownStyle = ComboBoxStyle.DropDownList;

                        List<Person> people = new List<Person>();

                        while (readerList.Read())
                        {
                            string fullName = $"{readerList["last_name"]} {readerList["first_name"]}";
                            int idPersonne = Convert.ToInt32(readerList["id_personne"]);

                            people.Add(new Person(idPersonne, fullName));
                        }

                        select_personne.DataSource = people;
                        select_personne.DisplayMember = "FullName";
                        select_personne.ValueMember = "Id";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            string priest = textPriestName.Text;
            string godfather = textGodFather.Text;
            DateTime date_confirmation = dateConfirmation.Value.Date;
            int id_personne;

            if (select_personne != null && select_personne.SelectedItem != null)
            {
                id_personne = ((Person)select_personne.SelectedItem).Id;
            }
            else
            {
                MessageBox.Show("La liste déroulante est vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(priest) || priest == "Entrez le nom du prêtre")
            {
                MessageBox.Show("Le champs nom du prêtre ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(godfather) || godfather == "Entrez le nom du parrain ou marraine")
            {
                MessageBox.Show("Le champs nom du parrain ou de la marraine ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (date_confirmation == null || dateConfirmation.Value == DateTime.MinValue)
            {
                MessageBox.Show("Veuillez sélectionner une date de confirmation.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string insertQuery = "INSERT INTO confirmation (priest, godfather, date_confirmation, id_personne) " +
                                        "VALUES (@priest, @godfather, @date_confirmation, @id_personne)";

                using (SQLiteCommand insertCommand = new SQLiteCommand(insertQuery, connection))
                {
                    insertCommand.Parameters.AddWithValue("@priest", priest);
                    insertCommand.Parameters.AddWithValue("@godfather", godfather);
                    insertCommand.Parameters.AddWithValue("@date_confirmation", date_confirmation);
                    insertCommand.Parameters.AddWithValue("@id_personne", id_personne);

                    int rowsAffected = insertCommand.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Les informations sur la Confirmation ont été insérées avec succès !", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Hide();

                        _formConfirmationReference.show_data_confirmation();
                    }
                    else
                    {
                        MessageBox.Show("Erreur lors de l'insertion des informations sur la  Confirmation", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
