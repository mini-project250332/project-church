﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Loginform;

namespace App
{
    public partial class Bapteme : Form
    {
        private Timer timer;
        private bool keyWasPressed = false;

        public Bapteme()
        {
            InitializeComponent();

            timer = new Timer();
            timer.Interval = 800;
            timer.Tick += Timer_Tick;

            textRecherche.KeyUp += TextRecherche_Key_up;
            textAnnee.KeyUp += TextRecherche_Key_up;
        }

        public static class EditBaptemeid
        {
            public static int Id { get; set; }
        }

        public void Open_Bapteme()
        {
            AjoutBapteme ajoutBapteme = new AjoutBapteme(this);
            ajoutBapteme.Show();

            ModifierBapteme modifierBapteme = new ModifierBapteme(this);
            modifierBapteme.Show();
        }

        private void Baptême_Load(object sender, EventArgs e)
        {
            searchMonth();

            show_data_bapteme();

            showhide_button_action();

            textRecherche.Text = "Recherche par nom ou prénom de la personne";
            textRecherche.ForeColor = Color.LightGray;
            textRecherche.BackColor = Color.White;

            textAnnee.Text = "Année";
            textAnnee.ForeColor = Color.LightGray;
            textAnnee.BackColor = Color.White;

            select_month.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void showhide_button_action()
        {
            string Profil = Session.Profil;

            if (Profil == "Administrateur")
            {
                button_ajouter.Visible = true;
                button_modifier.Visible = true;
                button_supprimer.Visible = true;
            }
            else
            {
                button_ajouter.Visible = false;
                button_modifier.Visible = false;
                button_supprimer.Visible = false;
                label_alert_user.Visible = true;
            }
        }

        private string GetSelectedRowID()
        {
            if (table_bapteme.SelectedRows.Count > 0)
            {
                string selectedID = table_bapteme.SelectedRows[0].Cells[0].Value.ToString();
                return selectedID;
            }
            else
            {
                return null;
            }
        }

        private void table_bapteme_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                DataGridViewRow selectedRow = table_bapteme.Rows[e.RowIndex];
                selectedRow.Selected = true;
            }
        }

        public void show_data_bapteme()
        {
            table_bapteme.Rows.Clear();
            table_bapteme.ReadOnly = true;

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT b.id_bapteme, p.last_name, p.first_name, b.date_bapteme FROM bapteme AS b " +
                    "INNER JOIN personne AS p ON p.id_personne = b.id_personne";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        while (readerList.Read())
                        {
                            object[] rowData = new object[readerList.FieldCount];
                            readerList.GetValues(rowData);

                            for (int i = 0; i < rowData.Length; i++)
                            {
                                if (rowData[i] is DateTime)
                                {
                                    rowData[i] = ((DateTime)rowData[i]).ToString("dd/MM/yyyy");
                                }
                            }

                            table_bapteme.Rows.Add(rowData);
                            table_bapteme.ClearSelection();
                        }
                    }
                }

                int nombreResultats = table_bapteme.Rows.Count;
                labelResultat.Text = "Résultat(s) : " + nombreResultats + " enregistré(s)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            keyWasPressed = false;

            string recherche = textRecherche.Text;
            string annee = textAnnee.Text;
            int selectedMonthId = (int)select_month.SelectedValue;

            if (!string.IsNullOrEmpty(recherche) || !string.IsNullOrEmpty(annee) || recherche != "Recherche par nom ou prénom de la personne" || annee != "Année" || selectedMonthId != 0)
            {
                rechercheBapteme(recherche, annee, selectedMonthId);
            }
            else
            {
                show_data_bapteme();
            }
        }

        private void TextRecherche_Key_up(object sender, KeyEventArgs e)
        {
            keyWasPressed = true;
            timer.Stop();
            timer.Start();
        }

        private void rechercheBapteme(string recherche, string annee, int month)
        {
            table_bapteme.Rows.Clear();
            table_bapteme.ReadOnly = true;

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT b.id_bapteme, p.last_name, p.first_name, b.date_bapteme FROM bapteme AS b " +
                    "INNER JOIN personne AS p ON p.id_personne = b.id_personne WHERE 1 = 1";

                if (!string.IsNullOrEmpty(recherche) && recherche != "Recherche par nom ou prénom de la personne")
                {
                    query += " AND (p.last_name LIKE @lastnameStartsWith OR p.first_name LIKE @firstnameStartsWith)";
                }

                if (!string.IsNullOrEmpty(annee) && annee != "Année")
                {
                    query += " AND strftime('%Y', b.date_bapteme) = @annee";
                }

                if (month != 0)
                {
                    query += " AND strftime('%m', b.date_bapteme) = @month";
                }

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    if (!string.IsNullOrEmpty(recherche) && recherche != "Recherche par nom ou prénom de la personne")
                    {
                        command.Parameters.AddWithValue("@lastnameStartsWith", "%" + recherche + "%");
                        command.Parameters.AddWithValue("@firstnameStartsWith", "%" + recherche + "%");
                    }

                    if (!string.IsNullOrEmpty(annee) && annee != "Année")
                    {
                        command.Parameters.AddWithValue("@annee", annee);
                    }

                    if (month != 0)
                    {
                        command.Parameters.AddWithValue("@month", month.ToString().PadLeft(2, '0'));
                    }

                    using (SQLiteDataReader readerList = command.ExecuteReader())
                    {
                        while (readerList.Read())
                        {
                            object[] rowData = new object[readerList.FieldCount];
                            readerList.GetValues(rowData);

                            for (int i = 0; i < rowData.Length; i++)
                            {
                                if (rowData[i] is DateTime)
                                {
                                    rowData[i] = ((DateTime)rowData[i]).ToString("dd/MM/yyyy");
                                }
                            }

                            table_bapteme.Rows.Add(rowData);
                            table_bapteme.ClearSelection();
                        }
                    }
                }

                int nombreResultats = table_bapteme.Rows.Count;
                labelResultat.Text = "Résultat(s) : " + nombreResultats + " enregistré(s)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textRecherche_Leave(object sender, EventArgs e)
        {
            if (textRecherche.Text == "")
            {
                textRecherche.Text = "Recherche par nom ou prénom de la personne";
                textRecherche.ForeColor = Color.LightGray;
                textRecherche.BackColor = Color.White;
            }
        }

        private void textRecherche_Enter(object sender, EventArgs e)
        {
            if (textRecherche.Text == "Recherche par nom ou prénom de la personne")
            {
                textRecherche.Text = "";
                textRecherche.ForeColor = Color.Black;
                textRecherche.BackColor = Color.WhiteSmoke;
            }
        }

        private void button_ajouter_Click(object sender, EventArgs e)
        {
            AjoutBapteme ajoutBapteme = new AjoutBapteme(this);
            ajoutBapteme.ShowDialog();
        }

        private void button_supprimer_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                DialogResult result = MessageBox.Show("Êtes-vous sûr de vouloir supprimer cette personne du baptême ?", "Confirmation de suppression", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    try
                    {
                        string deleteQuery = "DELETE FROM bapteme WHERE id_bapteme = @id";

                        using (SQLiteCommand deleteCommand = new SQLiteCommand(deleteQuery, connection))
                        {
                            deleteCommand.Parameters.AddWithValue("@id", selectedID);

                            int rowsAffected = deleteCommand.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                MessageBox.Show("La suppression de la personne dans le menu baptême a été effectuée avec succès !", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                show_data_bapteme();
                            }
                            else
                            {
                                MessageBox.Show("La suppression de la personne a échoué.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erreur lors de la suppression de la personne: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void button_modifier_Click(object sender, EventArgs e)
        {
            string selectedID = GetSelectedRowID();

            if (string.IsNullOrEmpty(selectedID))
            {
                MessageBox.Show("Aucune ligne sélectionnée.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                EditBaptemeid.Id = Convert.ToInt32(selectedID);

                ModifierBapteme modifierBapteme = new ModifierBapteme(this);
                modifierBapteme.ShowDialog(this);
            }
        }

        private void textAnnee_Enter(object sender, EventArgs e)
        {
            if (textAnnee.Text == "Année")
            {
                textAnnee.Text = "";
                textAnnee.ForeColor = Color.Black;
                textAnnee.BackColor = Color.WhiteSmoke;
            }
        }

        private void textAnnee_Leave(object sender, EventArgs e)
        {
            if (textAnnee.Text == "")
            {
                textAnnee.Text = "Année";
                textAnnee.ForeColor = Color.LightGray;
                textAnnee.BackColor = Color.White;
            }
        }

        private void textAnnee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void searchMonth()
        {
            Dictionary<int, string> mois = new Dictionary<int, string>
            {
                { 0, "Tous" },
                { 1, "Janvier" },
                { 2, "Février" },
                { 3, "Mars" },
                { 4, "Avril" },
                { 5, "Mai" },
                { 6, "Juin" },
                { 7, "Juillet" },
                { 8, "Août" },
                { 9, "Septembre" },
                { 10, "Octobre" },
                { 11, "Novembre" },
                { 12, "Décembre" }
            };

            select_month.DisplayMember = "Value";
            select_month.ValueMember = "Key";
            select_month.DataSource = new BindingSource(mois, null);
        }

        private void select_month_SelectedIndexChanged(object sender, EventArgs e)
        {
            string recherche = textRecherche.Text;
            string annee = textAnnee.Text;
            int selectedMonthId = (int)select_month.SelectedValue;

            if (!string.IsNullOrEmpty(recherche) || !string.IsNullOrEmpty(annee) || recherche != "Recherche par nom ou prénom de la personne" || annee != "Année" || selectedMonthId != 0)
            {
                rechercheBapteme(recherche, annee, selectedMonthId);
            }
            else
            {
                show_data_bapteme();
            }
        }
    }
}
