﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static App.Bapteme;

namespace App
{
    public partial class ModifierBapteme : Form
    {
        private bool isDragging = false;
        private Point lastCursor;
        private Point lastForm;

        private Bapteme _formBaptemeReference;

        public ModifierBapteme(Bapteme formBaptemeReference)
        {
            InitializeComponent();

            _formBaptemeReference = formBaptemeReference;
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button_retour_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void panel_moved_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursor = Cursor.Position;
                lastForm = this.Location;
            }
        }

        private void panel_moved_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point diff = Point.Subtract(Cursor.Position, new Size(lastCursor));
                this.Location = Point.Add(lastForm, new Size(diff));
            }
        }

        private void panel_moved_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void textPriestName_Enter(object sender, EventArgs e)
        {
            if (textPriestName.Text == "Entrez le nom du prêtre")
            {
                textPriestName.Text = "";
                textPriestName.ForeColor = Color.Black;
            }
        }

        private void textPriestName_Leave(object sender, EventArgs e)
        {
            if (textPriestName.Text == "")
            {
                textPriestName.Text = "Entrez le nom du prêtre";
                textPriestName.ForeColor = Color.LightGray;
            }
        }

        private void textGodFather_Enter(object sender, EventArgs e)
        {
            if (textGodFather.Text == "Entrez le nom du parrain ou marraine")
            {
                textGodFather.Text = "";
                textGodFather.ForeColor = Color.Black;
            }
        }

        private void textGodFather_Leave(object sender, EventArgs e)
        {
            if (textGodFather.Text == "")
            {
                textGodFather.Text = "Entrez le nom du parrain ou marraine";
                textGodFather.ForeColor = Color.LightGray;
            }
        }

        private void ModifierBapteme_Load(object sender, EventArgs e)
        {
            int id = EditBaptemeid.Id;

            show_Bapteme_by_id(id);
        }

        private void show_Bapteme_by_id(int id)
        {
            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "SELECT p.id_personne, b.id_bapteme, p.last_name, p.first_name, b.date_bapteme, b.priest, b.godfather FROM bapteme AS b " +
                    "INNER JOIN personne AS p ON p.id_personne = b.id_personne " +
                    "WHERE b.id_bapteme = @id";

                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", id);

                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        select_personne.Items.Clear();
                        select_personne.DropDownStyle = ComboBoxStyle.DropDownList;

                        List<Person> people = new List<Person>();

                        if (reader.Read())
                        {
                            int id_personne = Convert.ToInt32(reader["id_personne"]);
                            string first_name = reader["first_name"].ToString();
                            string last_name = reader["last_name"].ToString();
                            string priest = reader["priest"].ToString();
                            string godfather = reader["godfather"].ToString();
                            DateTime date_bapteme = Convert.ToDateTime(reader["date_bapteme"]);

                            string fullName = $"{last_name} {first_name}";
                            int idPersonne = Convert.ToInt32(id_personne);

                            people.Add(new Person(idPersonne, fullName));

                            select_personne.DataSource = people;
                            select_personne.DisplayMember = "FullName";
                            select_personne.ValueMember = "Id";

                            textPriestName.Text = priest;
                            textGodFather.Text = godfather;
                            select_personne.DropDownStyle = ComboBoxStyle.DropDownList;
                            dateBapteme.Value = date_bapteme;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_enregistrer_Click(object sender, EventArgs e)
        {
            int id = EditBaptemeid.Id;
            string priest = textPriestName.Text;
            string godfather = textGodFather.Text;
            DateTime date_bapteme = dateBapteme.Value.Date;
            int id_personne;

            if (select_personne != null && select_personne.SelectedItem != null)
            {
                id_personne = ((Person)select_personne.SelectedItem).Id;
            }
            else
            {
                MessageBox.Show("La liste déroulante est vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(priest) || priest == "Entrez le nom du prêtre")
            {
                MessageBox.Show("Le champs nom du prêtre ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(godfather) || godfather == "Entrez le nom du parrain ou marraine")
            {
                MessageBox.Show("Le champs nom du parrain ou de la marraine ne doit pas être vide.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (date_bapteme == null || dateBapteme.Value == DateTime.MinValue)
            {
                MessageBox.Show("Veuillez sélectionner une date de baptême.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            SQLiteConnection connection = SQLiteDatabaseConneciton.GetConnection();

            try
            {
                string query = "UPDATE bapteme SET priest = @priest, godfather = @godfather, date_bapteme = @date_bapteme," +
                    " id_personne = @id_personne WHERE id_bapteme = @id_bapteme";

                using (SQLiteCommand commandUpdate = new SQLiteCommand(query, connection))
                {

                    commandUpdate.Parameters.AddWithValue("@id_bapteme", id);
                    commandUpdate.Parameters.AddWithValue("@priest", priest);
                    commandUpdate.Parameters.AddWithValue("@godfather", godfather);
                    commandUpdate.Parameters.AddWithValue("@date_bapteme", date_bapteme);
                    commandUpdate.Parameters.AddWithValue("@id_personne", id_personne);

                    int rowsAffected = commandUpdate.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Modification de la Baptême avec succès", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.Hide();

                        _formBaptemeReference.show_data_bapteme();
                    }
                    else
                    {
                        MessageBox.Show("Erreur lors de la modification.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données: " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
